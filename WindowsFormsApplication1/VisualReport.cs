﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.medical.termo.data;
using System.Collections;

namespace WindowsFormsApplication1
{
    public partial class VisualReport : Form
    {
        public VisualReport(TermoSurvay survay)
        {
            InitializeComponent();
            this.pictureBox1.Image = (Image)survay.Termogram;
            var data = new Queue<Measure>(survay.Data);
            int k = 0, p = 0;
            bool isSet = false;
            try
            {
                while (k < survay.Cols)
                {
                    float[,] sensor = null;
                    for (int i = 0; i < survay.Rows - 1; i++)
                    {
                        if (sensor == null)
                            sensor = merge(survay.Data[p].getSensor(), survay.Data[p + 1].getSensor());
                        else
                            sensor = merge(sensor, survay.Data[p + 1].getSensor());
                        p++;
                    }
                    k++;
                    p++;
                    if (!isSet)
                    {
                        dataGridView1.ColumnCount = dataGridView1.ColumnCount + sensor.GetUpperBound(1) + 1;
                        isSet = true;
                    }
                    
                    for (int i = 0; i < sensor.GetLength(0); i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                        for (int j = 0; j < sensor.GetUpperBound(1) + 1; j++)
                        {
                            row.Cells[j].Value = sensor[i, j].ToString();
                        }
                        dataGridView1.Rows.Add(row);
                    }
                }
            }
            catch(Exception e)
            {

            }
        }
        float [,] merge(float[,] arr1, float[,] arr2)
        {
            int d2 = arr1.GetLength(1) + arr2.GetLength(1);
            int d1 = arr1.GetLength(0) > arr2.GetLength(0) ? arr1.GetLength(0) : arr2.GetLength(0);
            float[,] result = new float[d1, d2];

            AddToArray(result, arr1);
            AddToArray(result, arr2, arr1.GetLength(1));
            return result;
        }
        static void AddToArray(float[,] result, float[,] array, int start = 0)
        {
            for (int i = 0; i < array.GetLength(0); ++i)
            {
                for (int j = 0; j < array.GetLength(1); ++j)
                {
                    result[i, j + start] = array[i, j];
                }
            }
        }
    }
}
