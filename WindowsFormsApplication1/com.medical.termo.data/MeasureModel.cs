using System;
using System.Collections.Generic;
using System.Drawing;

namespace com.medical.termo.data
{
	public class MeasureModel
	{
		private float minTemp = -55f;

		private float maxTemp = 125f;

		private List<Measure> listOfMeasure = new List<Measure>();

		private Size measuresInDirection;

		public static string[] VertebraLevels = new string[]
		{
			"C1",
			"C2",
			"C3",
			"C4",
			"C5",
			"C6",
			"C7",
			"Th1",
			"Th2",
			"Th3",
			"Th4",
			"Th5",
			"Th6",
			"Th7",
			"Th8",
			"Th9",
			"Th10",
			"Th11",
			"Th12",
			"L1",
			"L2",
			"L3",
			"L4",
			"L5",
			"S1"
		};

		public float MaxTemp
		{
			get
			{
				return this.maxTemp;
			}
			set
			{
				this.maxTemp = value;
			}
		}

		public float MinTemp
		{
			get
			{
				return this.minTemp;
			}
			set
			{
				this.minTemp = value;
			}
		}

		public Size MeasuresInDirection
		{
			get
			{
				return this.measuresInDirection;
			}
			set
			{
				this.measuresInDirection = value;
			}
		}

		public List<Measure> Measures
		{
			get
			{
				return this.listOfMeasure;
			}
			set
			{
				this.listOfMeasure = value;
			}
		}

		public void addMeasure(Measure measure)
		{
			if (this.listOfMeasure.Contains(measure))
			{
				this.listOfMeasure.Remove(measure);
			}
			this.listOfMeasure.Add(measure);
			this.maxTemp = 125f;
			this.minTemp = -55f;
			foreach (Measure current in this.listOfMeasure)
			{
				if (current.MaxTemp > this.maxTemp || this.maxTemp == 125f)
				{
					this.maxTemp = current.MaxTemp;
				}
				if (current.MinTemp < this.minTemp || this.minTemp == -55f)
				{
					this.minTemp = current.MinTemp;
				}
			}
		}

		public bool isCellCanBeActive(Point point)
		{
			return point.Y <= this.measuresInDirection.Height && Math.Abs(point.X) <= this.measuresInDirection.Width / 2;
		}

		public void clearMeasures()
		{
			int i = 0;
			while (i < this.listOfMeasure.Count)
			{
				Point point = new Point(this.listOfMeasure[i].GridX, this.listOfMeasure[i].GridY);
				if (!this.isCellCanBeActive(point))
				{
					this.listOfMeasure.RemoveAt(i);
				}
				else
				{
					i++;
				}
			}
		}
	}
}
