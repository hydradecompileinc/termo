using com.medical.common.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Xml.Serialization;

namespace com.medical.termo.data
{
	[Serializable]
	public class TermoSurvay : Survay
	{
		private Size measuresInDirection;

        private int cols;
        private int rows;

        private Bitmap termogram;

		private TermoReport termoReport;

		private float environmentTemp;

		private float leftAxillaryTemp;

		private float rightAxillaryTemp;

        private List<Measure> result;
        [XmlElement]
        public List<Measure> Data
        {
            get
            {
                return result;
            }
            set
            {
                result = value;
            }
        }
        
        [XmlElement]
        public int Cols
        {
            get
            {
                return cols;
            }
            set
            {
                cols = value;
            }
        }

        [XmlElement]
        public int Rows
        {
            get
            {
                return rows;
            }
            set
            {
                rows = value;
            }
        }

        [XmlElement]
		public Size MeasuresInDirection
		{
			get
			{
				return this.measuresInDirection;
			}
			set
			{
				this.measuresInDirection = value;
			}
		}


		[XmlElement]
		public float EnvironmentTemp
		{
			get
			{
				return this.environmentTemp;
			}
			set
			{
				this.environmentTemp = value;
			}
		}

		[XmlElement]
		public float LeftAxillaryTemp
		{
			get
			{
				return this.leftAxillaryTemp;
			}
			set
			{
				this.leftAxillaryTemp = value;
			}
		}

		[XmlElement]
		public float RightAxillaryTemp
		{
			get
			{
				return this.rightAxillaryTemp;
			}
			set
			{
				this.rightAxillaryTemp = value;
			}
		}

		[XmlElement]
		public TermoReport TermoReport
		{
			get
			{
				return this.termoReport;
			}
			set
			{
				this.termoReport = value;
			}
		}

		[XmlElement]
		public byte[] XMLTermogram
		{
			get
			{
				TypeConverter converter = TypeDescriptor.GetConverter(typeof(Bitmap));
				return (byte[])converter.ConvertTo(this.termogram, typeof(byte[]));
			}
			set
			{
				this.termogram = new Bitmap(new MemoryStream(value));
			}
		}

		[XmlIgnore]
		public Bitmap Termogram
		{
			get
			{
				return this.termogram;
			}
			set
			{
				this.termogram = value;
			}
		}
	}
}
