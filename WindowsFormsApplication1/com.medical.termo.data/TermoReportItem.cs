using System;
using System.Xml.Serialization;

namespace com.medical.termo.data
{
	[Serializable]
	public class TermoReportItem
	{
		private int gridX;

		private int gridY;

		private float itemValue;

		[XmlElement]
		public int GridX
		{
			get
			{
				return this.gridX;
			}
			set
			{
				this.gridX = value;
			}
		}

		[XmlElement]
		public int GridY
		{
			get
			{
				return this.gridY;
			}
			set
			{
				this.gridY = value;
			}
		}

		[XmlElement]
		public float ItemValue
		{
			get
			{
				return this.itemValue;
			}
			set
			{
				this.itemValue = value;
			}
		}

		public TermoReportItem()
		{
		}

		public TermoReportItem(int gridX, int gridY, float itemValue)
		{
			this.gridX = gridX;
			this.gridY = gridY;
			this.itemValue = itemValue;
		}
	}
}
