using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml.Serialization;

namespace com.medical.termo.data
{
	[Serializable]
	public class TermoReport
	{
		private float deltaTemp;

		private float averageLeftTemp;

		private float averageRightTemp;

		private float averageAsymmetry;

		private List<TermoReportItem> listOfTemp = new List<TermoReportItem>();

		private List<TermoReportItem> listOfDeltaBetweenSymmetricPosition = new List<TermoReportItem>();

		private List<TermoReportItem> listOfDeltaBetweenPositionAndAverage = new List<TermoReportItem>();

		private List<TermoReportItem> listOfDeltaBetweenSymmetricSensor = new List<TermoReportItem>();

		private List<TermoReportItem> listOfDeltaBetweenSensorAndAverage = new List<TermoReportItem>();

		private List<TermoReportItem> listOfDeltaBetweenSensorAndPosition = new List<TermoReportItem>();

		private float maxLocalUpperLeft;

		private float maxLocalUpperRight;

		private float maxLocalAsymmetry;

		private Point cellMaxLocalUpperLeft;

		private Point cellMaxLocalUpperRight;

		private Point cellLocalAsymmetry;

		[XmlElement]
		public float DeltaTemp
		{
			get
			{
				return this.deltaTemp;
			}
			set
			{
				this.deltaTemp = value;
			}
		}

		[XmlElement]
		public float AverageLeftTemp
		{
			get
			{
				return this.averageLeftTemp;
			}
			set
			{
				this.averageLeftTemp = value;
			}
		}

		[XmlElement]
		public float AverageRightTemp
		{
			get
			{
				return this.averageRightTemp;
			}
			set
			{
				this.averageRightTemp = value;
			}
		}

		[XmlElement]
		public float AverageAsymmetry
		{
			get
			{
				return this.averageAsymmetry;
			}
			set
			{
				this.averageAsymmetry = value;
			}
		}

		[XmlElement]
		public List<TermoReportItem> ListOfTemp
		{
			get
			{
				return this.listOfTemp;
			}
			set
			{
				this.listOfTemp = value;
			}
		}

		[XmlElement]
		public List<TermoReportItem> ListOfDeltaBetweenSymmetricPosition
		{
			get
			{
				return this.listOfDeltaBetweenSymmetricPosition;
			}
			set
			{
				this.listOfDeltaBetweenSymmetricPosition = value;
			}
		}

		[XmlElement]
		public List<TermoReportItem> ListOfDeltaBetweenPositionAndAverage
		{
			get
			{
				return this.listOfDeltaBetweenPositionAndAverage;
			}
			set
			{
				this.listOfDeltaBetweenPositionAndAverage = value;
			}
		}

		[XmlElement]
		public List<TermoReportItem> ListOfDeltaBetweenSymmetricSensor
		{
			get
			{
				return this.listOfDeltaBetweenSymmetricSensor;
			}
			set
			{
				this.listOfDeltaBetweenSymmetricSensor = value;
			}
		}

		[XmlElement]
		public List<TermoReportItem> ListOfDeltaBetweenSensorAndAverage
		{
			get
			{
				return this.listOfDeltaBetweenSensorAndAverage;
			}
			set
			{
				this.listOfDeltaBetweenSensorAndAverage = value;
			}
		}

		[XmlElement]
		public List<TermoReportItem> ListOfDeltaBetweenSensorAndPosition
		{
			get
			{
				return this.listOfDeltaBetweenSensorAndPosition;
			}
			set
			{
				this.listOfDeltaBetweenSensorAndPosition = value;
			}
		}

		[XmlElement]
		public float MaxLocalUpperLeft
		{
			get
			{
				return this.maxLocalUpperLeft;
			}
			set
			{
				this.maxLocalUpperLeft = value;
			}
		}

		[XmlElement]
		public Point CellMaxLocalUpperLeft
		{
			get
			{
				return this.cellMaxLocalUpperLeft;
			}
			set
			{
				this.cellMaxLocalUpperLeft = value;
			}
		}

		[XmlElement]
		public float MaxLocalUpperRight
		{
			get
			{
				return this.maxLocalUpperRight;
			}
			set
			{
				this.maxLocalUpperRight = value;
			}
		}

		[XmlElement]
		public Point CellMaxLocalUpperRight
		{
			get
			{
				return this.cellMaxLocalUpperRight;
			}
			set
			{
				this.cellMaxLocalUpperRight = value;
			}
		}

		[XmlElement]
		public float MaxLocalAsymmetry
		{
			get
			{
				return this.maxLocalAsymmetry;
			}
			set
			{
				this.maxLocalAsymmetry = value;
			}
		}

		[XmlElement]
		public Point CellLocalAsymmetry
		{
			get
			{
				return this.cellLocalAsymmetry;
			}
			set
			{
				this.cellLocalAsymmetry = value;
			}
		}
	}
}
