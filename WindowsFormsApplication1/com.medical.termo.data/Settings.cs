using com.medical.common.data;
using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace com.medical.termo.data
{
	[Serializable]
	public class Settings
	{
		private const string DEFAULT_WORKDIRECTORY = "Документы Termo";

		private const int DEFAULT_TIMEOFMEASURE = 30;

		private const int DEFAULT_GRIDWIDTH = 2;

		private const int DEFAULT_GRIDHEIGHT = 2;

		private const string DEFAULT_VALUE = "Не задано";

		private const int SENSOR_IN_LINE = 6;

		private const int SENSOR_IN_COLUMN = 6;

		private string workDirectory;

		private int timeOfMeasure;

		private int gridWidth;

		private int gridHeight;

		private Doctor doctor;

		private string comPort;

		private float[][] calibrate;

		private static Settings current;

		private static string settingsPath = Application.StartupPath + "\\settings.xml";

		[XmlElement]
		public string WorkDirectory
		{
			get
			{
				return this.workDirectory;
			}
			set
			{
				this.workDirectory = value;
			}
		}

		[XmlElement]
		public int TimeOfMeasure
		{
			get
			{
				return this.timeOfMeasure;
			}
			set
			{
				this.timeOfMeasure = value;
			}
		}

		[XmlElement]
		public int GridWidth
		{
			get
			{
				return this.gridWidth;
			}
			set
			{
				this.gridWidth = value;
			}
		}

		[XmlElement]
		public int GridHeight
		{
			get
			{
				return this.gridHeight;
			}
			set
			{
				this.gridHeight = value;
			}
		}

		[XmlElement]
		public Doctor Doctor
		{
			get
			{
				return this.doctor;
			}
			set
			{
				this.doctor = value;
			}
		}

		[XmlElement]
		public string ComPort
		{
			get
			{
				return this.comPort;
			}
			set
			{
				this.comPort = value;
			}
		}

		[XmlArray]
		public float[][] Calibrate
		{
			get
			{
				return this.calibrate;
			}
			set
			{
				this.calibrate = value;
			}
		}

		[XmlIgnore]
		public int SensorInColumn
		{
			get
			{
				return 6;
			}
		}

		[XmlIgnore]
		public int SensorInLine
		{
			get
			{
				return 6;
			}
		}

		[XmlIgnore]
		public string DefaultValue
		{
			get
			{
				return "Не задано";
			}
		}

		public static string SettingsPath
		{
			get
			{
				return Settings.settingsPath;
			}
		}

		public static Settings Current
		{
			get
			{
				if (Settings.current == null)
				{
					Settings.current = Settings.Load();
				}
				return Settings.current;
			}
			set
			{
				Settings.current = value;
			}
		}

		private Settings()
		{
			this.workDirectory = this.getDefaultPath();
			this.timeOfMeasure = 30;
			this.gridWidth = 2;
			this.gridHeight = 2;
			this.doctor = new Doctor("Не задано", "Не задано", "Не задано", "Не задано");
			this.comPort = "Не задано";
			this.calibrate = new float[6][];
			for (int i = 0; i < this.calibrate.Length; i++)
			{
				this.calibrate[i] = new float[6];
			}
		}

		private static Settings Load()
		{
			Settings settings;
			if (File.Exists(Settings.settingsPath))
			{
				XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
				StreamReader streamReader = new StreamReader(Settings.settingsPath);
				settings = (Settings)xmlSerializer.Deserialize(streamReader);
				streamReader.Close();
			}
			else
			{
				settings = new Settings();
				Settings.Save(settings);
			}
			return settings;
		}

		public static void Save()
		{
			Settings.Save(Settings.current);
		}

		private static void Save(Settings settings)
		{
			if (settings != null)
			{
				XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
				StreamWriter streamWriter = new StreamWriter(Settings.settingsPath);
				xmlSerializer.Serialize(streamWriter, settings);
				streamWriter.Close();
			}
		}

		private string getDefaultPath()
		{
			string text = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\Документы Termo\\";
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			return text;
		}
	}
}
