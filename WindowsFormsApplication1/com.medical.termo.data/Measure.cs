using com.medical.termo.serial;
using System;
using System.Drawing;
using System.Xml.Serialization;

namespace com.medical.termo.data
{
    [Serializable]
	public class Measure : IEquatable<Measure>
	{
		public const float MAX_TEMP_SENSOR = 125f;

		public const float MIN_TEMP_SENSOR = -55f;

		private int gridX;

		private int gridY;

		private float[,] sensor = new float[Settings.Current.SensorInColumn, Settings.Current.SensorInLine];

		private float maxTemp = 125f;

		private float minTemp = -55f;

        public float[,] getSensor()
        {
            return sensor;
        }
        
        [XmlArray]
        [XmlArrayItem("measure")]
        public float [][] array
        {
            get
            {
                float[][] m = new float[Settings.Current.SensorInLine][];
                for(int i = 0; i < Settings.Current.SensorInLine; i++)
                {
                    m[i] = new float[Settings.Current.SensorInColumn];
                    for(int j = 0; j < Settings.Current.SensorInColumn; j++)
                    {
                        m[i][j] = sensor[i, j];
                    }
                }
                return m;
            }
            set
            {
                for(int i = 0; i < Settings.Current.SensorInColumn; i++)
                {
                    for(int j = 0; j < Settings.Current.SensorInLine; j++)
                    {
                        sensor[i, j] = value[i][j];
                    }
                }
            }
        }
        [XmlIgnore]
		public int GridX
		{
			get
			{
				return this.gridX;
			}
		}
        [XmlIgnore]
        public int GridY
		{
			get
			{
				return this.gridY;
			}
		}
        [XmlIgnore]
        public float MaxTemp
		{
			get
			{
				return this.maxTemp;
			}
		}
        [XmlIgnore]
        public float MinTemp
		{
			get
			{
				return this.minTemp;
			}
		}
        [XmlIgnore]
        public float AverageTemp
		{
			get
			{
				float num = 0f;
				for (int i = 0; i < Settings.Current.SensorInColumn; i++)
				{
					for (int j = 0; j < Settings.Current.SensorInLine; j++)
					{
						num += this.sensor[i, j];
					}
				}
				return num / (float)(Settings.Current.SensorInColumn * Settings.Current.SensorInLine);
			}
		}

		public Measure(int gridX, int gridY)
		{
			this.gridX = gridX;
			this.gridY = gridY;
		}
        public Measure()
        {

        }

		public static Measure createNewMeasure(Point activeCell, byte[] rawData, float[][] calibrate)
		{
			Measure measure = new Measure(activeCell.X, activeCell.Y);
			for (int i = 0; i < Settings.Current.SensorInLine * Settings.Current.SensorInColumn * DataCommand.BytePerSensor; i += DataCommand.BytePerSensor)
			{
				int num = (int)(rawData[i] - 1);
				int num2 = (int)(rawData[i + 1] - 1);
				measure.setSensor(num, num2, (short)(((int)rawData[i + 2] << 8) + (int)rawData[i + 3]), calibrate[num][num2]);
			}
			return measure;
		}

		public void setSensor(int x, int y, short value, float calibrate)
		{
			this.sensor[x, y] = this.codeToTemp(value) + calibrate;
			if (this.sensor[x, y] < this.minTemp || this.minTemp == -55f)
			{
				this.minTemp = this.sensor[x, y];
			}
			if (this.sensor[x, y] > this.maxTemp || this.maxTemp == 125f)
			{
				this.maxTemp = this.sensor[x, y];
			}
		}

		public float getSensor(int x, int y)
		{
			return this.sensor[x, y];
		}

		private float codeToTemp(short code)
		{
			return 180f * (float)code / 2880f;
		}

		public bool Equals(Measure other)
		{
			return this.gridX == other.gridX && this.gridY == other.gridY;
		}

		public override bool Equals(object obj)
		{
			bool result;
			if (obj == null)
			{
				result = base.Equals(obj);
			}
			else
			{
				Measure other = obj as Measure;
				result = this.Equals(other);
			}
			return result;
		}
	}
}
