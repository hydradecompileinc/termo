﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.medical.termo.serial;
using com.medical.termo.data;
namespace WindowsFormsApplication1
{
    public partial class SettingsForm : Form
    {
        private float[][] compensation;
        public SettingsForm()
        {
            InitializeComponent();
            this.setToolTips(new ToolTip());
            this.doctorSecondNameTextBox.Text = com.medical.termo.data.Settings.Current.Doctor.SecondName;
            this.doctorFirstNameTextBox.Text = com.medical.termo.data.Settings.Current.Doctor.FirstName;
            this.doctorMiddleNameTextBox.Text = com.medical.termo.data.Settings.Current.Doctor.MiddleName;
            this.hospitalTextBox.Text = com.medical.termo.data.Settings.Current.Doctor.Hospital;
            this.gridWidthNumericUpDown.Value = com.medical.termo.data.Settings.Current.GridWidth;
            this.gridHeightNumericUpDown.Value = com.medical.termo.data.Settings.Current.GridHeight;
            this.timeOfMeasureNumericUpDown.Value = com.medical.termo.data.Settings.Current.TimeOfMeasure;
            this.workDirectoryTextBox.Text = com.medical.termo.data.Settings.Current.WorkDirectory;
            this.comPortTextBox.Text = com.medical.termo.data.Settings.Current.ComPort;
            if (com.medical.termo.data.Settings.Current.ComPort.Equals(com.medical.termo.data.Settings.Current.DefaultValue))
            {
                this.calibrateButton.Enabled = false;
            }
            this.compensation = new float[com.medical.termo.data.Settings.Current.SensorInLine][];
            for (int i = 0; i < com.medical.termo.data.Settings.Current.SensorInLine; i++)
            {
                this.compensation[i] = new float[com.medical.termo.data.Settings.Current.SensorInColumn];
            }
        }
        private void setToolTips(ToolTip toolTip)
        {
            toolTip.SetToolTip(this.comPortLookupButton, "Поиск COM-порта, к которому подключено устройство");
            toolTip.SetToolTip(this.workDirectoryLookupButton, "Выбор рабочей директории");
            toolTip.SetToolTip(this.cancelButton, "Отмена");
            toolTip.SetToolTip(this.okButton, "Подтверждение настроек");
            toolTip.SetToolTip(this.gridWidthNumericUpDown, "Ширина решетки замеров");
            toolTip.SetToolTip(this.gridHeightNumericUpDown, "Высота решетки замеров");
            toolTip.SetToolTip(this.comPortTextBox, "COM-порт устройства");
            toolTip.SetToolTip(this.timeOfMeasureNumericUpDown, "Интервал от момента зауска блока датчиков до замера температуры");
            toolTip.SetToolTip(this.calibrateButton, "Калибровка устройства");
        }
        private void okButton_Click(object sender, EventArgs e)
        {
            com.medical.termo.data.Settings.Current.Doctor.SecondName = this.doctorSecondNameTextBox.Text;
            com.medical.termo.data.Settings.Current.Doctor.FirstName = this.doctorFirstNameTextBox.Text;
            com.medical.termo.data.Settings.Current.Doctor.MiddleName = this.doctorMiddleNameTextBox.Text;
            com.medical.termo.data.Settings.Current.Doctor.Hospital = this.hospitalTextBox.Text;
            com.medical.termo.data.Settings.Current.GridWidth = (int)this.gridWidthNumericUpDown.Value;
            com.medical.termo.data.Settings.Current.GridHeight = (int)this.gridHeightNumericUpDown.Value;
            com.medical.termo.data.Settings.Current.TimeOfMeasure = (int)this.timeOfMeasureNumericUpDown.Value;
            com.medical.termo.data.Settings.Current.WorkDirectory = this.workDirectoryTextBox.Text;
            com.medical.termo.data.Settings.Current.ComPort = this.comPortTextBox.Text;
            com.medical.termo.data.Settings.Current.Calibrate = this.compensation;
        }

        private void comPortLookupButton_Click(object sender, EventArgs e)
        {
            ComPortLookupForm comPortLookupForm = new ComPortLookupForm();
            comPortLookupForm.ShowDialog();
            string devicePortName = comPortLookupForm.DevicePortName;
            if (devicePortName == string.Empty)
            {
                MessageBox.Show(this, "Устройство не найдено. Подключите устройство, выключите и включите питание устройства или смените порт подключения.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                MessageBox.Show(this, "Устройство подключено к порту " + devicePortName + ". Не забудьте выполнить калибровку устройства.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                this.comPortTextBox.Text = devicePortName;
                this.calibrateButton.Enabled = true;
            }
        }

        private void workDirectoryLookupButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                this.workDirectoryTextBox.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void SettingsForm_Validating(object sender, CancelEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox.Text == string.Empty)
            {
                this.settingsFormErrorProvider.SetError(sender as Control, "Поле не может быть пустым");
                e.Cancel = true;
            }
            else
            {
                this.settingsFormErrorProvider.SetError(sender as Control, string.Empty);
                e.Cancel = false;
            }
        }

        private void calibrateButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show(this, "Поместите блок датчиков вдали от источников тепла или в герметичный бокс и подождите не менее 10 мин. Затем нажимте \"Ок\"", "Сообщение", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            if (dialogResult == DialogResult.OK)
            {
                this.compensation = this.calibrate();
            }
        }
        public float[][] calibrate()
        {
            float[][] array = new float[com.medical.termo.data.Settings.Current.SensorInLine][];
            for (int i = 0; i < com.medical.termo.data.Settings.Current.SensorInLine; i++)
            {
                array[i] = new float[com.medical.termo.data.Settings.Current.SensorInColumn];
            }
            SerialDriver serialDriver = new SerialDriver(this.comPortTextBox.Text);
            serialDriver.open();
            try
            {
                byte[] array2 = serialDriver.readData(1000);
                if (array2 == null)
                {
                    throw new Exception("Ошибка калибровки");
                }
                Measure measure = Measure.createNewMeasure(new Point(0, 0), array2, array);
                for (int i = 0; i < com.medical.termo.data.Settings.Current.SensorInLine; i++)
                {
                    for (int j = 0; j < com.medical.termo.data.Settings.Current.SensorInColumn; j++)
                    {
                        array[i][j] = measure.AverageTemp - measure.getSensor(i, j);
                    }
                }
                MessageBox.Show(this, "Калибровка успешно выполнена.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                serialDriver.close();
            }
            return array;
        }
    }
}
