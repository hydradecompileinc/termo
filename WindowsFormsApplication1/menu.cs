﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using com.medical.common.data;
using com.medical.termo.data;
using com.medical.termo.engine;
namespace WindowsFormsApplication1
{
    public partial class menu : Form
    {
        private Project project;
        private workpanel workPanel;
        public menu()
        {
            InitializeComponent();
            workPanel = new workpanel();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                workPanel = new workpanel();
                workPanel.resetPanel(0);
                this.project = null;
                workPanel.Project = this.project;
                //this.updateNextPrevButtonsState();
                workPanel.Show();
            }
            catch (Exception ex)
            {
                //MessageBoxForm.show(this, ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                MessageBox.Show(ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.InitialDirectory = com.medical.termo.data.Settings.Current.WorkDirectory;
                openFileDialog.Filter = "Project files (*.xml)|*.xml";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(Project), new Type[]
					{
						typeof(TermoSurvay)
					});
                    StreamReader streamReader = new StreamReader(openFileDialog.FileName);
                    this.workPanel.Project = (Project)xmlSerializer.Deserialize(streamReader);
                    streamReader.Close();
                    this.workPanel.resetPanel(2);
                    TermoSurvay survay = (TermoSurvay)workPanel.Project.Survays[0];
                    VisualReport report = new VisualReport(survay);
                    report.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<Project> list = new List<Project>();
            try
            {
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
                folderBrowserDialog.SelectedPath = com.medical.termo.data.Settings.Current.WorkDirectory;
                folderBrowserDialog.Description = "Выберите папку с файлами обследований";
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.InitialDirectory = com.medical.termo.data.Settings.Current.WorkDirectory;
                    saveFileDialog.DefaultExt = "xls";
                    saveFileDialog.FileName = "Report";
                    saveFileDialog.Filter = "Файлы Microsoft Excel (*.xls)|*.xls";
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        string[] files = Directory.GetFiles(folderBrowserDialog.SelectedPath, "*.xml", SearchOption.AllDirectories);
                        string[] array = files;
                        for (int i = 0; i < array.Length; i++)
                        {
                            string path = array[i];
                            try
                            {
                                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Project), new Type[]
								{
									typeof(TermoSurvay)
								});
                                StreamReader streamReader = new StreamReader(path);
                                Project item = (Project)xmlSerializer.Deserialize(streamReader);
                                list.Add(item);
                                streamReader.Close();
                            }
                            catch (Exception var_8_10C)
                            {
                            }
                        }
                        ExportReportEngine exportReportEngine = new ExportReportEngine();
                        exportReportEngine.export(list, saveFileDialog.FileName);
                        MessageBox.Show(this, "Файл успешно сохранен в папку " + Path.GetDirectoryName(saveFileDialog.FileName), "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(new ProcessStartInfo
                {
                    FileName = Application.StartupPath + "\\Termo.pdf"
                });
            }
            catch (Exception var_1_28)
            {
                MessageBox.Show(this, "Не удалось открыть файл помощи. Попробуйте просмотреть файл Termo.pdf сторонней программой", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                SettingsForm settingsForm = new SettingsForm();
                if (settingsForm.ShowDialog() == DialogResult.OK)
                {
                    com.medical.termo.data.Settings.Save();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
