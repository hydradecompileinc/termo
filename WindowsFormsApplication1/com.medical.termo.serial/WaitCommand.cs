using System;

namespace com.medical.termo.serial
{
	internal class WaitCommand : Command
	{
		public WaitCommand()
		{
			this.cmd[0] = 85;
			this.cmd[1] = 170;
			this.cmd[2] = 171;
			this.data = new byte[0];
		}
	}
}
