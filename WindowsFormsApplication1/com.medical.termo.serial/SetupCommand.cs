using System;

namespace com.medical.termo.serial
{
	internal class SetupCommand : Command
	{
		public SetupCommand()
		{
			this.cmd[0] = 85;
			this.cmd[1] = 170;
			this.cmd[2] = 205;
			this.data = new byte[3];
		}
	}
}
