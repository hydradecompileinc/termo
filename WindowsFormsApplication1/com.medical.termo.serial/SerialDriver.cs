using System;
using System.IO.Ports;
using System.Threading;

namespace com.medical.termo.serial
{
	public class SerialDriver
	{
		private const int BAUDE_RATE = 19200;

		private const int READ_TIMEOUT = 0;

		private const int WRITE_TIMEOUT = 10000;

		private const int PAUSE_TIMEOUT = 50;

		private const int WAIT_FOR_DATA_FROM_DEVICE_TIMEOUT = 1000;

		private const int POLL_INTERVAL = 10;

		private SerialPort comPort;

		public string ComPortName
		{
			get
			{
				return this.comPort.PortName;
			}
		}

		public SerialDriver(string comPortName)
		{
			this.comPort = new SerialPort(comPortName);
			this.comPort.BaudRate = 19200;
			this.comPort.ReadTimeout = 0;
			this.comPort.WriteTimeout = 10000;
			this.comPort.ReadBufferSize = 1024;
			this.comPort.WriteBufferSize = 1024;
		}

		public void open()
		{
			this.comPort.Open();
		}

		public void close()
		{
			if (this.comPort != null && this.comPort.IsOpen)
			{
				this.comPort.Close();
			}
		}

		public void init()
		{
			SetupCommand obj = new SetupCommand();
			InitCommand command = new InitCommand();
			this.writeCommand(command);
			Thread.Sleep(50);
			SetupCommand setupCommand = new SetupCommand();
			this.readCommand(setupCommand);
			if (!setupCommand.Equals(obj))
			{
				throw new Exception("Init operation failed");
			}
		}

		public void accept()
		{
			bool waitCommandReceived = false;
			SerialDataReceivedEventHandler value = delegate(object sender, SerialDataReceivedEventArgs e)
			{
				WaitCommand obj = new WaitCommand();
				try
				{
					WaitCommand waitCommand = new WaitCommand();
					this.readCommand(waitCommand);
					if (waitCommand.Equals(obj))
					{
						waitCommandReceived = true;
					}
				}
				catch (TimeoutException var_2_35)
				{
				}
			};
			this.comPort.DataReceived += value;
			while (!waitCommandReceived)
			{
				Thread.Sleep(10);
			}
			this.comPort.DataReceived -= value;
		}

		public byte[] readData(int timeOut)
		{
			bool dataCommandReceived = false;
			byte[] rawData = null;
			this.writeCommand(new ReadCommand());
			SerialDataReceivedEventHandler value = delegate(object sender, SerialDataReceivedEventArgs e)
			{
				DataCommand obj = new DataCommand();
				try
				{
					if (!dataCommandReceived)
					{
						Thread.Sleep(1000);
						DataCommand dataCommand = new DataCommand();
						this.readCommand(dataCommand);
						if (dataCommand.Equals(obj))
						{
							rawData = dataCommand.Data;
							dataCommandReceived = true;
						}
					}
				}
				catch (TimeoutException var_2_58)
				{
				}
			};
			this.comPort.DataReceived += value;
			int num = timeOut;
			while (!dataCommandReceived)
			{
				if (timeOut != 0)
				{
					Thread.Sleep(10);
					num--;
					if (num == 0)
					{
						dataCommandReceived = true;
					}
				}
			}
			this.comPort.DataReceived -= value;
			return rawData;
		}

		public static string comPortLookup()
		{
			SerialDriver serialDriver = null;
			string result = string.Empty;
			InitCommand initCommand = new InitCommand();
			SetupCommand setupCommand = new SetupCommand();
			string[] portNames = SerialPort.GetPortNames();
			string[] array = portNames;
			for (int i = 0; i < array.Length; i++)
			{
				string text = array[i];
				try
				{
					serialDriver = new SerialDriver(text);
					serialDriver.open();
					serialDriver.init();
					result = text;
				}
				catch (Exception var_6_4B)
				{
				}
				finally
				{
					try
					{
						if (serialDriver != null)
						{
							serialDriver.close();
						}
					}
					catch (Exception var_6_4B)
					{
					}
				}
				Thread.Sleep(50);
			}
			return result;
		}

		private void writeCommand(Command command)
		{
			this.comPort.Write(command.Cmd, 0, command.Cmd.Length);
			this.comPort.Write(command.Data, 0, command.Data.Length);
		}

		private void readCommand(Command command)
		{
			byte[] array = new byte[command.Length];
			this.comPort.Read(array, 0, array.Length);
			command.Create(array);
		}
	}
}
