using System;

namespace com.medical.termo.serial
{
	internal class InitCommand : Command
	{
		public InitCommand()
		{
			this.cmd[0] = 85;
			this.cmd[1] = 170;
			this.cmd[2] = 252;
			this.data = new byte[0];
		}
	}
}
