using System;


namespace com.medical.termo.serial
{
	public abstract class Command
	{
		protected const int CMD_LEN = 3;

		protected byte[] cmd = new byte[3];

		protected byte[] data;

		public int Length
		{
			get
			{
				return this.cmd.Length + this.data.Length;
			}
		}

		public byte[] Data
		{
			get
			{
				return this.data;
			}
		}

		public byte[] Cmd
		{
			get
			{
				return this.cmd;
			}
		}

		public void Create(byte[] rawByte)
		{
			Array.Copy(rawByte, this.cmd, this.cmd.Length);
			Array.Copy(rawByte, this.cmd.Length, this.data, 0, rawByte.Length - this.cmd.Length);
		}

		public override bool Equals(object obj)
		{
			bool result;
			if (!(obj is Command))
			{
				result = false;
			}
			else
			{
				Command command = obj as Command;
				if (this.cmd.Length != command.Cmd.Length)
				{
					result = false;
				}
				else
				{
					bool flag = true;
					for (int i = 0; i < this.cmd.Length; i++)
					{
						if (this.cmd[i] != command.Cmd[i])
						{
							flag = false;
							break;
						}
					}
					result = flag;
				}
			}
			return result;
		}
	}
}
