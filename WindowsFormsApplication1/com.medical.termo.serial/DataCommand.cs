using com.medical.termo.data;
using System;

namespace com.medical.termo.serial
{
	public class DataCommand : Command
	{
		private const int BYTE_PER_SENSOR = 4;

		private const int BYTE_PER_CRC = 1;

		public static int BytePerSensor
		{
			get
			{
				return 4;
			}
		}

		public DataCommand()
		{
			this.cmd[0] = 85;
			this.cmd[1] = 170;
			this.cmd[2] = 188;
			this.data = new byte[Settings.Current.SensorInColumn * Settings.Current.SensorInLine * DataCommand.BytePerSensor + 1];
		}
	}
}
