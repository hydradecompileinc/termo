using System;

namespace com.medical.termo.serial
{
	internal class ReadCommand : Command
	{
		public ReadCommand()
		{
			this.cmd[0] = 85;
			this.cmd[1] = 170;
			this.cmd[2] = 218;
			this.data = new byte[0];
		}
	}
}
