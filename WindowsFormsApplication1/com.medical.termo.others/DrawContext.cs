using System;
using System.Drawing;

namespace com.medical.termo.components
{
	public abstract class DrawContext
	{
		private int verticalBorder;

		private int horizontalBorder;

		private Size bounds;

		public int VerticalBorder
		{
			get
			{
				return this.verticalBorder;
			}
			set
			{
				this.verticalBorder = value;
			}
		}

		public int HorizontalBorder
		{
			get
			{
				return this.horizontalBorder;
			}
			set
			{
				this.horizontalBorder = value;
			}
		}

		public Size Bounds
		{
			get
			{
				return this.bounds;
			}
			set
			{
				this.bounds = value;
			}
		}

		public DrawContext(int verticalBorder, int horizontalBorder, Size bounds)
		{
			this.verticalBorder = verticalBorder;
			this.horizontalBorder = horizontalBorder;
			this.bounds = bounds;
		}
	}
}
