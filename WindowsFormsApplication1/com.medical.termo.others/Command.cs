using System;
using System.Windows.Forms;

namespace com.medical.termo.forms
{
	public abstract class Command
	{
		protected Form form;

		protected Command(Form form)
		{
			this.form = form;
		}

		public abstract void doIt();
	}
}
