﻿namespace WindowsFormsApplication1
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.workDiectoryLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.workDirectoryLookupButton = new System.Windows.Forms.Button();
            this.systemGroupBox = new System.Windows.Forms.GroupBox();
            this.comPortTextBox = new System.Windows.Forms.TextBox();
            this.calibrateButton = new System.Windows.Forms.Button();
            this.comPortLabel = new System.Windows.Forms.Label();
            this.timeOfMeasureLabel = new System.Windows.Forms.Label();
            this.gtridHeightLabel = new System.Windows.Forms.Label();
            this.gridWidthLabel = new System.Windows.Forms.Label();
            this.comPortLookupButton = new System.Windows.Forms.Button();
            this.gridWidthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.gridHeightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.timeOfMeasureNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.doctorGroupBox = new System.Windows.Forms.GroupBox();
            this.doctorSecondNameTextBox = new System.Windows.Forms.TextBox();
            this.doctorFirstNameTextBox = new System.Windows.Forms.TextBox();
            this.doctorMiddleNameTextBox = new System.Windows.Forms.TextBox();
            this.doctorSecondNameLabel = new System.Windows.Forms.Label();
            this.doctorFirstNameLabel = new System.Windows.Forms.Label();
            this.doctorMiddleNameLabel = new System.Windows.Forms.Label();
            this.hospitalLabel = new System.Windows.Forms.Label();
            this.hospitalTextBox = new System.Windows.Forms.TextBox();
            this.workDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.settingsFormErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.systemGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWidthNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHeightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeOfMeasureNumericUpDown)).BeginInit();
            this.doctorGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsFormErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // workDiectoryLabel
            // 
            this.workDiectoryLabel.AutoSize = true;
            this.workDiectoryLabel.Location = new System.Drawing.Point(12, 257);
            this.workDiectoryLabel.Name = "workDiectoryLabel";
            this.workDiectoryLabel.Size = new System.Drawing.Size(167, 15);
            this.workDiectoryLabel.TabIndex = 9;
            this.workDiectoryLabel.Text = "Путь к рабочей директории";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(265, 304);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(55, 45);
            this.cancelButton.TabIndex = 13;
            this.cancelButton.Text = "Назад";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(191, 304);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(55, 45);
            this.okButton.TabIndex = 12;
            this.okButton.Text = "Далее";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // workDirectoryLookupButton
            // 
            this.workDirectoryLookupButton.Location = new System.Drawing.Point(415, 260);
            this.workDirectoryLookupButton.Name = "workDirectoryLookupButton";
            this.workDirectoryLookupButton.Size = new System.Drawing.Size(75, 45);
            this.workDirectoryLookupButton.TabIndex = 11;
            this.workDirectoryLookupButton.Text = "Открыть файл";
            this.workDirectoryLookupButton.UseVisualStyleBackColor = true;
            this.workDirectoryLookupButton.Click += new System.EventHandler(this.workDirectoryLookupButton_Click);
            // 
            // systemGroupBox
            // 
            this.systemGroupBox.Controls.Add(this.comPortTextBox);
            this.systemGroupBox.Controls.Add(this.calibrateButton);
            this.systemGroupBox.Controls.Add(this.comPortLabel);
            this.systemGroupBox.Controls.Add(this.timeOfMeasureLabel);
            this.systemGroupBox.Controls.Add(this.gtridHeightLabel);
            this.systemGroupBox.Controls.Add(this.gridWidthLabel);
            this.systemGroupBox.Controls.Add(this.comPortLookupButton);
            this.systemGroupBox.Controls.Add(this.gridWidthNumericUpDown);
            this.systemGroupBox.Controls.Add(this.gridHeightNumericUpDown);
            this.systemGroupBox.Controls.Add(this.timeOfMeasureNumericUpDown);
            this.systemGroupBox.Location = new System.Drawing.Point(294, 12);
            this.systemGroupBox.Name = "systemGroupBox";
            this.systemGroupBox.Size = new System.Drawing.Size(201, 234);
            this.systemGroupBox.TabIndex = 8;
            this.systemGroupBox.TabStop = false;
            this.systemGroupBox.Text = "Параметры системы";
            // 
            // comPortTextBox
            // 
            this.comPortTextBox.Location = new System.Drawing.Point(25, 141);
            this.comPortTextBox.Name = "comPortTextBox";
            this.comPortTextBox.ReadOnly = true;
            this.comPortTextBox.Size = new System.Drawing.Size(151, 20);
            this.comPortTextBox.TabIndex = 10;
            // 
            // calibrateButton
            // 
            this.calibrateButton.Location = new System.Drawing.Point(99, 167);
            this.calibrateButton.Name = "calibrateButton";
            this.calibrateButton.Size = new System.Drawing.Size(77, 45);
            this.calibrateButton.TabIndex = 9;
            this.calibrateButton.Text = "Калибровка";
            this.calibrateButton.UseVisualStyleBackColor = true;
            this.calibrateButton.Click += new System.EventHandler(this.calibrateButton_Click);
            // 
            // comPortLabel
            // 
            this.comPortLabel.AutoSize = true;
            this.comPortLabel.Location = new System.Drawing.Point(22, 124);
            this.comPortLabel.Name = "comPortLabel";
            this.comPortLabel.Size = new System.Drawing.Size(116, 15);
            this.comPortLabel.TabIndex = 6;
            this.comPortLabel.Text = "Номер COM-порта";
            // 
            // timeOfMeasureLabel
            // 
            this.timeOfMeasureLabel.AutoSize = true;
            this.timeOfMeasureLabel.Location = new System.Drawing.Point(21, 93);
            this.timeOfMeasureLabel.Name = "timeOfMeasureLabel";
            this.timeOfMeasureLabel.Size = new System.Drawing.Size(103, 15);
            this.timeOfMeasureLabel.TabIndex = 4;
            this.timeOfMeasureLabel.Text = "Время замера, с";
            // 
            // gtridHeightLabel
            // 
            this.gtridHeightLabel.AutoSize = true;
            this.gtridHeightLabel.Location = new System.Drawing.Point(123, 28);
            this.gtridHeightLabel.Name = "gtridHeightLabel";
            this.gtridHeightLabel.Size = new System.Drawing.Size(51, 15);
            this.gtridHeightLabel.TabIndex = 2;
            this.gtridHeightLabel.Text = "Высота";
            // 
            // gridWidthLabel
            // 
            this.gridWidthLabel.AutoSize = true;
            this.gridWidthLabel.Location = new System.Drawing.Point(21, 28);
            this.gridWidthLabel.Name = "gridWidthLabel";
            this.gridWidthLabel.Size = new System.Drawing.Size(53, 15);
            this.gridWidthLabel.TabIndex = 0;
            this.gridWidthLabel.Text = "Ширина";
            // 
            // comPortLookupButton
            // 
            this.comPortLookupButton.Location = new System.Drawing.Point(25, 168);
            this.comPortLookupButton.Name = "comPortLookupButton";
            this.comPortLookupButton.Size = new System.Drawing.Size(72, 45);
            this.comPortLookupButton.TabIndex = 8;
            this.comPortLookupButton.Text = "Поиск";
            this.comPortLookupButton.UseVisualStyleBackColor = true;
            this.comPortLookupButton.Click += new System.EventHandler(this.comPortLookupButton_Click);
            // 
            // gridWidthNumericUpDown
            // 
            this.gridWidthNumericUpDown.Location = new System.Drawing.Point(24, 45);
            this.gridWidthNumericUpDown.Name = "gridWidthNumericUpDown";
            this.gridWidthNumericUpDown.ReadOnly = true;
            this.gridWidthNumericUpDown.Size = new System.Drawing.Size(50, 20);
            this.gridWidthNumericUpDown.TabIndex = 1;
            // 
            // gridHeightNumericUpDown
            // 
            this.gridHeightNumericUpDown.Location = new System.Drawing.Point(126, 44);
            this.gridHeightNumericUpDown.Name = "gridHeightNumericUpDown";
            this.gridHeightNumericUpDown.ReadOnly = true;
            this.gridHeightNumericUpDown.Size = new System.Drawing.Size(50, 20);
            this.gridHeightNumericUpDown.TabIndex = 3;
            // 
            // timeOfMeasureNumericUpDown
            // 
            this.timeOfMeasureNumericUpDown.Location = new System.Drawing.Point(126, 91);
            this.timeOfMeasureNumericUpDown.Name = "timeOfMeasureNumericUpDown";
            this.timeOfMeasureNumericUpDown.ReadOnly = true;
            this.timeOfMeasureNumericUpDown.Size = new System.Drawing.Size(50, 20);
            this.timeOfMeasureNumericUpDown.TabIndex = 5;
            // 
            // doctorGroupBox
            // 
            this.doctorGroupBox.Controls.Add(this.doctorSecondNameTextBox);
            this.doctorGroupBox.Controls.Add(this.doctorFirstNameTextBox);
            this.doctorGroupBox.Controls.Add(this.doctorMiddleNameTextBox);
            this.doctorGroupBox.Controls.Add(this.doctorSecondNameLabel);
            this.doctorGroupBox.Controls.Add(this.doctorFirstNameLabel);
            this.doctorGroupBox.Controls.Add(this.doctorMiddleNameLabel);
            this.doctorGroupBox.Controls.Add(this.hospitalLabel);
            this.doctorGroupBox.Controls.Add(this.hospitalTextBox);
            this.doctorGroupBox.Location = new System.Drawing.Point(12, 12);
            this.doctorGroupBox.Name = "doctorGroupBox";
            this.doctorGroupBox.Size = new System.Drawing.Size(268, 233);
            this.doctorGroupBox.TabIndex = 7;
            this.doctorGroupBox.TabStop = false;
            this.doctorGroupBox.Text = "Информация о враче";
            // 
            // doctorSecondNameTextBox
            // 
            this.doctorSecondNameTextBox.Location = new System.Drawing.Point(18, 44);
            this.doctorSecondNameTextBox.Name = "doctorSecondNameTextBox";
            this.doctorSecondNameTextBox.Size = new System.Drawing.Size(230, 20);
            this.doctorSecondNameTextBox.TabIndex = 1;
            // 
            // doctorFirstNameTextBox
            // 
            this.doctorFirstNameTextBox.Location = new System.Drawing.Point(18, 90);
            this.doctorFirstNameTextBox.Name = "doctorFirstNameTextBox";
            this.doctorFirstNameTextBox.Size = new System.Drawing.Size(230, 20);
            this.doctorFirstNameTextBox.TabIndex = 3;
            // 
            // doctorMiddleNameTextBox
            // 
            this.doctorMiddleNameTextBox.Location = new System.Drawing.Point(18, 141);
            this.doctorMiddleNameTextBox.Name = "doctorMiddleNameTextBox";
            this.doctorMiddleNameTextBox.Size = new System.Drawing.Size(230, 20);
            this.doctorMiddleNameTextBox.TabIndex = 5;
            // 
            // doctorSecondNameLabel
            // 
            this.doctorSecondNameLabel.AutoSize = true;
            this.doctorSecondNameLabel.Location = new System.Drawing.Point(15, 28);
            this.doctorSecondNameLabel.Name = "doctorSecondNameLabel";
            this.doctorSecondNameLabel.Size = new System.Drawing.Size(62, 15);
            this.doctorSecondNameLabel.TabIndex = 0;
            this.doctorSecondNameLabel.Text = "Фамилия";
            // 
            // doctorFirstNameLabel
            // 
            this.doctorFirstNameLabel.AutoSize = true;
            this.doctorFirstNameLabel.Location = new System.Drawing.Point(15, 74);
            this.doctorFirstNameLabel.Name = "doctorFirstNameLabel";
            this.doctorFirstNameLabel.Size = new System.Drawing.Size(32, 15);
            this.doctorFirstNameLabel.TabIndex = 2;
            this.doctorFirstNameLabel.Text = "Имя";
            // 
            // doctorMiddleNameLabel
            // 
            this.doctorMiddleNameLabel.AutoSize = true;
            this.doctorMiddleNameLabel.Location = new System.Drawing.Point(15, 125);
            this.doctorMiddleNameLabel.Name = "doctorMiddleNameLabel";
            this.doctorMiddleNameLabel.Size = new System.Drawing.Size(63, 15);
            this.doctorMiddleNameLabel.TabIndex = 4;
            this.doctorMiddleNameLabel.Text = "Отчество";
            // 
            // hospitalLabel
            // 
            this.hospitalLabel.AutoSize = true;
            this.hospitalLabel.Location = new System.Drawing.Point(15, 177);
            this.hospitalLabel.Name = "hospitalLabel";
            this.hospitalLabel.Size = new System.Drawing.Size(135, 15);
            this.hospitalLabel.TabIndex = 6;
            this.hospitalLabel.Text = "Лечебное учреждение";
            // 
            // hospitalTextBox
            // 
            this.hospitalTextBox.Location = new System.Drawing.Point(18, 193);
            this.hospitalTextBox.Name = "hospitalTextBox";
            this.hospitalTextBox.Size = new System.Drawing.Size(230, 20);
            this.hospitalTextBox.TabIndex = 7;
            // 
            // workDirectoryTextBox
            // 
            this.workDirectoryTextBox.Location = new System.Drawing.Point(12, 273);
            this.workDirectoryTextBox.Name = "workDirectoryTextBox";
            this.workDirectoryTextBox.Size = new System.Drawing.Size(379, 20);
            this.workDirectoryTextBox.TabIndex = 10;
            // 
            // settingsFormErrorProvider
            // 
            this.settingsFormErrorProvider.ContainerControl = this;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 366);
            this.Controls.Add(this.workDiectoryLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.workDirectoryLookupButton);
            this.Controls.Add(this.systemGroupBox);
            this.Controls.Add(this.doctorGroupBox);
            this.Controls.Add(this.workDirectoryTextBox);
            this.Name = "SettingsForm";
            this.Text = "Termo 2";
            this.Validating += new System.ComponentModel.CancelEventHandler(this.SettingsForm_Validating);
            this.systemGroupBox.ResumeLayout(false);
            this.systemGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWidthNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHeightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeOfMeasureNumericUpDown)).EndInit();
            this.doctorGroupBox.ResumeLayout(false);
            this.doctorGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsFormErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label workDiectoryLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button workDirectoryLookupButton;
        private System.Windows.Forms.GroupBox systemGroupBox;
        private System.Windows.Forms.TextBox comPortTextBox;
        private System.Windows.Forms.Button calibrateButton;
        private System.Windows.Forms.Label comPortLabel;
        private System.Windows.Forms.Label timeOfMeasureLabel;
        private System.Windows.Forms.Label gtridHeightLabel;
        private System.Windows.Forms.Label gridWidthLabel;
        private System.Windows.Forms.Button comPortLookupButton;
        private System.Windows.Forms.NumericUpDown gridWidthNumericUpDown;
        private System.Windows.Forms.NumericUpDown gridHeightNumericUpDown;
        private System.Windows.Forms.NumericUpDown timeOfMeasureNumericUpDown;
        private System.Windows.Forms.GroupBox doctorGroupBox;
        private System.Windows.Forms.TextBox doctorSecondNameTextBox;
        private System.Windows.Forms.TextBox doctorFirstNameTextBox;
        private System.Windows.Forms.TextBox doctorMiddleNameTextBox;
        private System.Windows.Forms.Label doctorSecondNameLabel;
        private System.Windows.Forms.Label doctorFirstNameLabel;
        private System.Windows.Forms.Label doctorMiddleNameLabel;
        private System.Windows.Forms.Label hospitalLabel;
        private System.Windows.Forms.TextBox hospitalTextBox;
        private System.Windows.Forms.TextBox workDirectoryTextBox;
        private System.Windows.Forms.ErrorProvider settingsFormErrorProvider;
    }
}