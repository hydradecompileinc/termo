﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.medical.termo.serial;
namespace WindowsFormsApplication1
{
    public partial class ComPortLookupForm : Form
    {
        private delegate string LookupDelegate();
        private string devicePortName;
        public string DevicePortName
        {
            get
            {
                return this.devicePortName;
            }
        }
        public ComPortLookupForm()
        {
            InitializeComponent();
            ComPortLookupForm.LookupDelegate lookupDelegate = new ComPortLookupForm.LookupDelegate(SerialDriver.comPortLookup);
            IAsyncResult asyncResult = lookupDelegate.BeginInvoke(new AsyncCallback(this.callBackMethod), lookupDelegate);
        }
        private void callBackMethod(IAsyncResult result)
        {
            ComPortLookupForm.LookupDelegate lookupDelegate = (ComPortLookupForm.LookupDelegate)result.AsyncState;
            this.devicePortName = lookupDelegate.EndInvoke(result);
            base.BeginInvoke(new MethodInvoker(delegate
            {
                base.Close();
            }));
        }   
    }
}
