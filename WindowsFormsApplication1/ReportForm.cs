﻿using com.medical.termo.engine;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Termo.Properties;

namespace WindowsFormsApplication1
{
    public partial class ReportForm : Form
    {
        private const int SIZE = 550;
        private const int BORDER = 20;
        private const int BUTTON_WIDTH = 55;
        private const int BUTTON_HEIGHT = 45;
        private Color BUTTON_COLOR = Color.FromArgb(212, 208, 200);
        private DrawReportEngine drawReportEngine;
        
        public ReportForm()
        {
            InitializeComponent();
        }

        public DrawReportEngine DrawReportEngine
        {
            get
            {
                return this.drawReportEngine;
            }
            set
            {
                this.drawReportEngine = value;
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void ReportForm_Paint(object sender, PaintEventArgs e)
        {
            Control control = sender as Control;
            e.Graphics.TranslateTransform((float)base.AutoScrollPosition.X, (float)base.AutoScrollPosition.Y);
            float y = this.drawReportEngine.drawReport(e.Graphics, control.Width).Y;
            base.AutoScrollMinSize = new Size(550, (int)y + 40);
        }
    }
}
