﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using com.medical.common.data;
using com.medical.termo.components;
using com.medical.termo.data;
using com.medical.termo.serial;
using com.medical.termo.engine;
using Termo.Properties;
namespace WindowsFormsApplication1
{
    public partial class workpanel : Form
    {
        public workpanel()
        {
            InitializeComponent();
            this.controlList.Add(Action.SURVAY);
            this.controlList.Add(Action.MEASURE);
            this.controlList.Add(Action.REPORT);
            this.controlList.Add(Action.SAVE_AND_EXPORT);
            this.measureModel = createNewMeasureModel();
            // SurvayControl
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.timerLabel.Text = string.Empty;
            this.upperLevelComboBox.Items.AddRange(MeasureModel.VertebraLevels);
            this.lowerLevelComboBox.Items.AddRange(MeasureModel.VertebraLevels);
            this.upperLevelComboBox.SelectedItem = "L1";
            this.lowerLevelComboBox.SelectedItem = "S1";
            this.activeCell = new Point(1, 1);
            this.gridWidthNumericUpDown.Value = com.medical.termo.data.Settings.Current.GridWidth;
            this.gridHeightNumericUpDown.Value = com.medical.termo.data.Settings.Current.GridHeight;
            this.timeOfmeasureNumericUpDown.Value = com.medical.termo.data.Settings.Current.TimeOfMeasure;
            this.measureModel = this.createNewMeasureModel();
        }
        private enum Action
        {
            SURVAY,
            MEASURE,
            REPORT,
            SAVE_AND_EXPORT
        }
        private int currentControl = 0;
        private int startAction = 0;
        private bool lastPage = false;
        private bool activityTrigger = false;
        private List<Action> controlList = new List<Action>();
        public event EventHandler ActivityStarted;
        public event EventHandler ActivityStopped;
        public event EventHandler SurvayEnded;
        private Thread measureThread;
        private Project project;
        private Project survayCopy;
        private SerialDriver serialDriver;
        private MeasureModel measureModel = new MeasureModel();
        private const char DEGREE = '°';
        private const int PLOT_BORDER = 15;
        private const int BORDER_WIDTH = 2;
        private const int INTERNAL_BORDER_WIDTH = 1;
        private const int ACTIVE_CELL_BORDER_WIDTH = 2;
        private const int ACTIVE_CELL_BORDER_PADDING = 1;
        private const int LABEL_TEXT_FONT_SIZE = 8;
        private const int LEVEL_TEXT_FONT_SIZE = 6;
        private const int TEMP_TEXT_FONT_SIZE_SMALL = 4;
        private const int TEMP_TEXT_FONT_SIZE_BIG = 8;
        private const float PRECISION = 0.25f;
        private FontFamily TEMP_FONT_FAMILY = FontFamily.GenericSerif;
        private FontFamily LABEL_FONT_FAMILY = FontFamily.GenericSerif;
        private Color BACK_COLOR = Color.White;
        private Color COLD_COLOR = Color.Cyan;
        private Color BORDER_COLOR = Color.Black;
        private Color ACTIVE_CELL_BORDER_COLOR = Color.Yellow;
        private Color LABEL_TEXT_COLOR = Color.Red;
        private Color TEMP_TEXT_COLOR = Color.Black;
        private Point activeCell;
        private ReportForm reportForm;
        public SerialDriver SerialDriver
        {
            get { return this.serialDriver; }
            set { this.serialDriver = value; }
        }
        public Thread MeasureThread
        {
            get
            {
                return this.measureThread;
            }
        }
        public MeasureModel getMeasureModel()
        {
            return this.measureModel;
        }
        public Project gProject
        {
            get
            {
                Sex sex;
                if (this.sexMaleRadioButton.Checked)
                {
                    sex = Sex.MALE;
                }
                else
                {
                    sex = Sex.FEMALE;
                }
                this.project.Patient = new Patient(this.patientFirstNameTextBox.Text, this.patientSecondNameTextBox.Text, this.patientMiddleNameTextBox.Text, this.dateOfBirthDateTimePicker.Value, sex, (float)this.weightNumericUpDown.Value, (int)this.heightNumericUpDown.Value);
                this.project.PreliminaryDiagnosis = this.preliminaryDiagnosisTextBox.Text;
                if (this.project.Survays.Count == 0)
                {
                    TermoSurvay termoSurvay = new TermoSurvay();
                    termoSurvay.Date = DateTime.Now;
                    termoSurvay.Doctor = new Doctor(this.doctorFirstNameTextBox.Text, this.doctorSecondNameTextBox.Text, this.doctorMiddleNameTextBox.Text, this.hospitalTextBox.Text);
                    termoSurvay.EnvironmentTemp = (float)this.environmentTempNumericUpDown.Value;
                    termoSurvay.LeftAxillaryTemp = (float)this.leftAxillaryTempNumericUpDown.Value;
                    termoSurvay.RightAxillaryTemp = (float)this.rightAxillaryTempNumericUpDown.Value;
                    this.project.Survays.Add(termoSurvay);
                }
                else
                {
                    this.project.Survays[0].Date = DateTime.Now;
                    this.project.Survays[0].Doctor = new Doctor(this.doctorFirstNameTextBox.Text, this.doctorSecondNameTextBox.Text, this.doctorMiddleNameTextBox.Text, this.hospitalTextBox.Text);
                    ((TermoSurvay)this.project.Survays[0]).EnvironmentTemp = (float)this.environmentTempNumericUpDown.Value;
                    ((TermoSurvay)this.project.Survays[0]).LeftAxillaryTemp = (float)this.leftAxillaryTempNumericUpDown.Value;
                    ((TermoSurvay)this.project.Survays[0]).RightAxillaryTemp = (float)this.rightAxillaryTempNumericUpDown.Value;
                }
                return this.project;
            }
            set
            {
                this.project = value;
                if (this.project.PreliminaryDiagnosis != null)
                {
                    this.preliminaryDiagnosisTextBox.Text = this.project.PreliminaryDiagnosis;
                }
                else
                {
                    this.preliminaryDiagnosisTextBox.Text = com.medical.termo.data.Settings.Current.DefaultValue;
                }
                if (this.project.Patient != null)
                {
                    this.patientFirstNameTextBox.Text = this.project.Patient.FirstName;
                    this.patientSecondNameTextBox.Text = this.project.Patient.SecondName;
                    this.patientMiddleNameTextBox.Text = this.project.Patient.MiddleName;
                    if (this.project.Patient.Sex == Sex.MALE)
                    {
                        this.sexMaleRadioButton.Checked = true;
                        this.sexFemaleRadioButton.Checked = false;
                    }
                    else
                    {
                        this.sexMaleRadioButton.Checked = false;
                        this.sexFemaleRadioButton.Checked = true;
                    }
                    this.dateOfBirthDateTimePicker.Value = this.project.Patient.DateOfBirth;
                    this.weightNumericUpDown.Value = (decimal)this.project.Patient.Weight;
                    this.heightNumericUpDown.Value = this.project.Patient.Height;
                }
                else
                {
                    this.patientFirstNameTextBox.Text = string.Empty;
                    this.patientSecondNameTextBox.Text = string.Empty;
                    this.patientMiddleNameTextBox.Text = string.Empty;
                    this.dateOfBirthDateTimePicker.Value = DateTime.Now;
                    this.sexMaleRadioButton.Checked = true;
                    this.weightNumericUpDown.Value = 60m;
                    this.heightNumericUpDown.Value = 170;
                }
                if (this.project.Survays != null && this.project.Survays.Count == 1)
                {
                    this.environmentTempNumericUpDown.Value = (decimal)((TermoSurvay)this.project.Survays[0]).EnvironmentTemp;
                    this.leftAxillaryTempNumericUpDown.Value = (decimal)((TermoSurvay)this.project.Survays[0]).LeftAxillaryTemp;
                    this.rightAxillaryTempNumericUpDown.Value = (decimal)((TermoSurvay)this.project.Survays[0]).RightAxillaryTemp;
                    if (this.project.Survays[0].Doctor != null)
                    {
                        this.doctorFirstNameTextBox.Text = this.project.Survays[0].Doctor.FirstName;
                        this.doctorSecondNameTextBox.Text = this.project.Survays[0].Doctor.SecondName;
                        this.doctorMiddleNameTextBox.Text = this.project.Survays[0].Doctor.MiddleName;
                        this.hospitalTextBox.Text = this.project.Survays[0].Doctor.Hospital;
                    }
                }
                else
                {
                    this.doctorSecondNameTextBox.Text = com.medical.termo.data.Settings.Current.Doctor.SecondName;
                    this.doctorFirstNameTextBox.Text = com.medical.termo.data.Settings.Current.Doctor.FirstName;
                    this.doctorMiddleNameTextBox.Text = com.medical.termo.data.Settings.Current.Doctor.MiddleName;
                    this.hospitalTextBox.Text = com.medical.termo.data.Settings.Current.Doctor.Hospital;
                    this.environmentTempNumericUpDown.Value = 25m;
                    this.leftAxillaryTempNumericUpDown.Value = 36.6m;
                    this.rightAxillaryTempNumericUpDown.Value = 36.6m;
                }
            }
        }
        public Project Project
        {
            get
            {
                return this.project;
            }
            set
            {
                this.project = value;
            }
        }
        private delegate void LockDelegate(bool lokk);
        private class MeasureControlDrawContext : DrawContext
        {
            private Size drawGridSize;

            private Size cellSize;

            public Size DrawGridSize
            {
                get
                {
                    return this.drawGridSize;
                }
                set
                {
                    this.drawGridSize = value;
                }
            }

            public Size CellSize
            {
                get
                {
                    return this.cellSize;
                }
                set
                {
                    this.cellSize = value;
                }
            }

            public MeasureControlDrawContext(int verticalBorder, int horizontalBorder, Size bounds, Size drawGridSize, Size cellSize)
                : base(verticalBorder, horizontalBorder, bounds)
            {
                this.drawGridSize = drawGridSize;
                this.cellSize = cellSize;
            }
        }
        public void resetPanel(int action)
        {
            this.startAction = action;
            this.currentControl = action;
            this.doForwardAction(this.controlList[this.currentControl]);
            this.workPanelTabControl.SelectedIndex = this.currentControl;
        }
        public void nextControl()
        {
            this.button1.Enabled = true;
            if (this.hasNextControl())
            {
                this.currentControl++;
                if (this.currentControl == this.workPanelTabControl.TabPages.Count)
                {
                    //this.SurvayEnded(this, new EventArgs());
                    this.project = null;
                    this.createNewMeasureModel();
                }
                else
                {
                    try
                    {
                        this.button2.Enabled = true;
                        this.doForwardAction(this.controlList[this.currentControl]);
                        if (this.currentControl == this.workPanelTabControl.TabPages.Count - 1)
                            this.button2.Enabled = false;
                        else
                            this.button2.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        this.currentControl--;
                        this.doBackwardAction(this.controlList[this.currentControl]);
                        //throw ex;
                    }
                    this.workPanelTabControl.SelectedIndex = this.currentControl;
                }
            }
        }
        public void prevControl()
        {
            this.button2.Enabled = true;
            if (this.hasPrevControl())
            {
                this.currentControl--;
                try
                {
                    this.doBackwardAction(this.controlList[this.currentControl]);
                    if (currentControl == 0)
                        this.button1.Enabled = false;
                    else
                        this.button1.Enabled = true;
                }
                catch (Exception ex)
                {
                    this.currentControl++;
                    this.doForwardAction(this.controlList[this.currentControl]);
                    throw ex;
                }
                this.workPanelTabControl.SelectedIndex = this.currentControl;
            }
        }
        private void doForwardAction(Action action)
        {
            switch (action)
            {
                case Action.SURVAY:
                    if (this.project == null)
                    {
                        this.project = new Project();
                    }
                    this.survayCopy = this.gProject; // this.survayControl.Project = this.project
                    this.createNewMeasureModel();
                    break;
                case Action.MEASURE:
                    this.gProject = this.survayCopy;//this.project = this.survayControl.Project;
                    //this.ActivityStarted(this, new EventArgs());
                    this.activityTrigger = true;
                    this.measureThreadStart();
                    break;
                case Action.REPORT:
                    {
                        if (this.MeasureThread != null && this.measureThread.IsAlive)
                        {
                            this.measureThread.Abort();
                        }
                        if (this.activityTrigger)
                        {
                            this.activityTrigger = false;
                            //this.ActivityStopped(this, new EventArgs());
                        }
                        TermoSurvay termoSurvay = (TermoSurvay)this.project.Survays[0];
                        if (this.startAction == 0)
                        {
                            termoSurvay.Termogram = this.getTermogram(new Size(420, 420), 10, 20);
                            CreateReportEngine createReportEngine = new CreateReportEngine(this.getMeasureModel());
                            termoSurvay.TermoReport = createReportEngine.createReport();
                            termoSurvay.MeasuresInDirection = this.getMeasureModel().MeasuresInDirection;
                        }
                        this.termogramPictureBox.Image = termoSurvay.Termogram;
                        this.reportForm = new ReportForm();
                        this.reportForm.DrawReportEngine = new DrawReportEngine(termoSurvay.TermoReport);
                        this.reportForm.Location = new Point(base.Width / 2, 0);
                        this.reportForm.Show();
                        break;
                    }
                case Action.SAVE_AND_EXPORT:
                    this.closeForm(this.reportForm);
                    if (this.project.FinalDiagnosis == null)
                    {
                        this.project.FinalDiagnosis = com.medical.termo.data.Settings.Current.DefaultValue;
                    }
                    this.finalDiagnosisTextBox.Text = this.project.FinalDiagnosis;
                    break;
            }
        }

        private void doBackwardAction(Action action)
        {
            switch (action)
            {
                case Action.SURVAY:
                    if (this.MeasureThread != null && this.measureThread.IsAlive)
                    {
                        this.measureThread.Abort();
                    }
                    if (this.activityTrigger)
                    {
                        this.activityTrigger = false;
                        //this.ActivityStopped(this, new EventArgs());
                    }
                    break;
                case Action.MEASURE:
                    this.closeForm(this.reportForm); 
                    //this.ActivityStarted(this, new EventArgs());
                    this.activityTrigger = true;
                    this.measureThreadStart();
                    break;
            }
        }
        private void closeForm(Form form)
        {
            if (form != null)
            {
                form.Close();
            }
        }

        private void measureThreadStart()
        {
            this.serialDriver = new SerialDriver(com.medical.termo.data.Settings.Current.ComPort);
            //this.measureControl.SerialDriver = this.serialDriver;
            this.serialDriver.open();
            this.measureThread = new Thread(new ThreadStart(this.measureThreadAction));
            this.measureThread.Start();
        }

        public bool hasNextControl()
        {
            return (this.currentControl < this.workPanelTabControl.TabPages.Count && this.startAction == 0) || (this.currentControl < this.workPanelTabControl.TabPages.Count - 1 && this.startAction != 0);
        }

        public bool hasPrevControl()
        {
            return this.currentControl > this.startAction;
        }
        public MeasureModel createNewMeasureModel()
        {
            this.measureModel = new MeasureModel();
            this.measureModel.MeasuresInDirection = new Size((int)this.gridWidthNumericUpDown.Value, (int)this.gridHeightNumericUpDown.Value);
            return this.measureModel;
        }
        public void measureThreadAction()
        {
            try
            {
                while (true)
                {
                    this.serialDriver.accept();
                    base.BeginInvoke(new LockDelegate(this.setLockPropertiesDialogs), new object[]
					{
						false
					});
                    int time;
                    for (time = (int)this.timeOfmeasureNumericUpDown.Value; time >= 0; time--)
                    {
                        base.BeginInvoke(new MethodInvoker(delegate
                        {
                            this.timerLabel.Text = string.Format("{0,4:000} c", time);
                            this.timerLabel.Refresh();
                        }));
                        Thread.Sleep(1000);
                    }
                    base.BeginInvoke(new MethodInvoker(delegate
                    {
                        this.timerLabel.Text = "Чтение";
                        this.timerLabel.Refresh();
                    }));
                    byte[] array = this.serialDriver.readData(0);
                    base.BeginInvoke(new MethodInvoker(delegate
                    {
                        this.timerLabel.Text = string.Empty;
                        this.timerLabel.Refresh();
                    }));
                    if (array != null)
                    {
                        Measure measure = Measure.createNewMeasure(this.activeCell, array, com.medical.termo.data.Settings.Current.Calibrate);
                        this.measureModel.addMeasure(measure);
                    }
                    base.BeginInvoke(new LockDelegate(this.setLockPropertiesDialogs), new object[]
					{
						true
					});
                }
            }
            catch (ThreadAbortException var_4_156)
            {
                this.serialDriver.close();
                //MessageBox.Show("Ошибка в MainFrame.MesureThreadAction" + var_4_156.Message);
            }
        }
        private void setLockPropertiesDialogs(bool lokk)
        {
            this.gridWidthNumericUpDown.Enabled = lokk;
            this.gridHeightNumericUpDown.Enabled = lokk;
            this.timeOfmeasureNumericUpDown.Enabled = lokk;
            this.plotPanel.Enabled = lokk;
            this.tempCheckBox.Enabled = lokk;
            this.upperLevelComboBox.Enabled = lokk;
            this.lowerLevelComboBox.Enabled = lokk;
            //this.scale.MinTemp = this.measureModel.MinTemp;
            //this.scale.MaxTemp = this.measureModel.MaxTemp;
            this.Refresh();
        }
        public Bitmap getTermogram(Size size, int fontSize, int border)
        {
            TermogramEngine termogramEngine = new TermogramEngine(size);
            Graphics termoGraphics = termogramEngine.getTermoGraphics();
            MeasureControlDrawContext drawContext = this.getDrawContext(this.measureModel, termogramEngine.getTermoSize(), border);
            this.drawCells(termoGraphics, this.measureModel, drawContext);
            this.drawMeasures(termoGraphics, this.measureModel, drawContext, this.tempCheckBox.Checked);
            Image vertebra = Image.FromFile("Vertebra.png");
            this.drawTransparentImage(termoGraphics, drawContext, vertebra, 1f, this.upperLevelComboBox.SelectedIndex, this.lowerLevelComboBox.SelectedIndex, true);
            this.drawLabels(termoGraphics, this.measureModel, drawContext, fontSize);
            this.drawGrid(termoGraphics, drawContext);
            Graphics scaleGraphics = termogramEngine.getScaleGraphics();
            Size scaleSize = termogramEngine.getScaleSize();
            //this.scale.drawScale(scaleGraphics, termogramEngine.GraphBorder, fontSize, scaleSize);
            return termogramEngine.getGraph();
        }
        private MeasureControlDrawContext getDrawContext(MeasureModel measureModel, Size bounds, int border)
        {
            Size drawSize = this.getDrawSize(measureModel.MeasuresInDirection);
            Size cellSize = this.getCellSize(measureModel.MeasuresInDirection, bounds, border);
            return new MeasureControlDrawContext(border, border, bounds, drawSize, cellSize);
        }
        private Size getDrawSize(Size gridSize)
        {
            Size result;
            if (gridSize.Width == 2 && gridSize.Height <= 2)
            {
                result = new Size(2, 2);
            }
            else
            {
                result = new Size(4, 4);
            }
            return result;
        }

        private Size getCellSize(Size measureInDirection, Size bounds, int border)
        {
            Size drawSize = this.getDrawSize(measureInDirection);
            int num = (bounds.Width - 2 * border) / drawSize.Width;
            int height = num;
            return new Size(num, height);
        }
        private void plotPanel_Paint(object sender, PaintEventArgs e)
        {
            Control control = sender as Control;
            Size size = control.Size;
            MeasureControlDrawContext drawContext = this.getDrawContext(this.measureModel, size, 15);
            this.drawCells(e.Graphics, this.measureModel, drawContext);
            this.drawMeasures(e.Graphics, this.measureModel, drawContext, this.tempCheckBox.Checked);
            //Image vertebra = Resources.Vertebra;
            Image vertebra = Image.FromFile("Vertebra.png"); 
            this.drawTransparentImage(e.Graphics, drawContext, vertebra, 1f, this.upperLevelComboBox.SelectedIndex, this.lowerLevelComboBox.SelectedIndex, false);
            this.drawLabels(e.Graphics, this.measureModel, drawContext, 8);
            this.drawGrid(e.Graphics, drawContext);
            this.drawActiveCell(e.Graphics, this.measureModel, drawContext, this.activeCell);
        }
        private void drawCells(Graphics g, MeasureModel measureModel, MeasureControlDrawContext drawContext)
        {
            for (int i = -drawContext.DrawGridSize.Width / 2; i <= drawContext.DrawGridSize.Width / 2; i++)
            {
                if (i != 0)
                {
                    for (int j = 1; j <= drawContext.DrawGridSize.Height; j++)
                    {
                        Point point = new Point(i, j);
                        Color backColor = this.BACK_COLOR;
                        if (measureModel.isCellCanBeActive(point))
                        {
                            backColor = this.COLD_COLOR;
                        }
                        this.drawCell(g, point, drawContext, backColor);
                    }
                }
            }
        }

        private void drawGrid(Graphics g, MeasureControlDrawContext drawContext)
        {
            for (int i = -drawContext.DrawGridSize.Width / 2; i <= drawContext.DrawGridSize.Width / 2; i++)
            {
                if (i != 0)
                {
                    for (int j = 1; j <= drawContext.DrawGridSize.Height; j++)
                    {
                        Point grid = new Point(i, j);
                        this.drawCell(g, grid, drawContext);
                    }
                }
            }
        }

        private void drawCell(Graphics g, Point grid, MeasureControlDrawContext drawContext, Color backColor)
        {
            Brush brush = new SolidBrush(backColor);
            Point point = this.gridToPoint(grid, drawContext);
            g.FillRectangle(brush, drawContext.HorizontalBorder + point.X, drawContext.VerticalBorder + point.Y, drawContext.CellSize.Width, drawContext.CellSize.Height);
        }

        private void drawCell(Graphics g, Point grid, MeasureControlDrawContext drawContext)
        {
            Pen pen = new Pen(this.BORDER_COLOR, 2f);
            Point point = this.gridToPoint(grid, drawContext);
            g.DrawRectangle(pen, drawContext.HorizontalBorder + point.X, drawContext.VerticalBorder + point.Y, drawContext.CellSize.Width, drawContext.CellSize.Height);
        }

        private void drawLabels(Graphics g, MeasureModel measureModel, MeasureControlDrawContext drawContext, int fontSize)
        {
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            Brush brush = new SolidBrush(this.LABEL_TEXT_COLOR);
            Font font = new Font(this.LABEL_FONT_FAMILY, (float)fontSize);
            for (int i = -drawContext.DrawGridSize.Width / 2; i <= drawContext.DrawGridSize.Width / 2; i++)
            {
                if (i != 0)
                {
                    string s = Math.Abs(i).ToString();
                    Point point = this.gridToPoint(new Point(i, 0), drawContext);
                    int num = drawContext.HorizontalBorder + point.X + drawContext.CellSize.Width / 2;
                    g.DrawString(s, font, brush, (float)num, (float)(drawContext.VerticalBorder / 2), stringFormat);
                    g.DrawString(s, font, brush, (float)num, (float)(drawContext.VerticalBorder + drawContext.CellSize.Height * drawContext.DrawGridSize.Height + drawContext.VerticalBorder / 2), stringFormat);
                }
            }
            for (int j = 1; j <= drawContext.DrawGridSize.Height; j++)
            {
                string s2 = ((char)(65 + j - 1)).ToString();
                Point point2 = this.gridToPoint(new Point(0, j), drawContext);
                int num2 = drawContext.VerticalBorder + point2.Y + drawContext.CellSize.Height / 2;
                g.DrawString(s2, font, brush, (float)(drawContext.HorizontalBorder / 2), (float)num2, stringFormat);
            }
        }

        private void drawMeasures(Graphics g, MeasureModel measureModel, MeasureControlDrawContext drawContext, bool showTemp)
        {
            Pen pen = new Pen(this.BORDER_COLOR, 1f);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            int num = drawContext.CellSize.Width / com.medical.termo.data.Settings.Current.SensorInLine;
            int num2 = drawContext.CellSize.Height / com.medical.termo.data.Settings.Current.SensorInColumn;
            foreach (Measure current in measureModel.Measures)
            {
                Point grid = new Point(current.GridX, current.GridY);
                Font font = new Font(this.TEMP_FONT_FAMILY, 8f);
                float width = g.MeasureString("##,##", font).Width;
                if (width > (float)num)
                {
                    font = new Font(this.TEMP_FONT_FAMILY, 4f);
                }
                for (int i = 0; i < com.medical.termo.data.Settings.Current.SensorInLine; i++)
                {
                    for (int j = 0; j < com.medical.termo.data.Settings.Current.SensorInColumn; j++)
                    {
                        float sensor = current.getSensor(i, j);
                        float temp = this.tempToTemp(sensor, 0.25f, measureModel);
                        Brush brush = new SolidBrush(this.tempToColor(temp, measureModel.MinTemp, measureModel.MaxTemp));
                        Point point = this.gridToPoint(grid, drawContext);
                        int num3 = drawContext.HorizontalBorder + i * num + point.X;
                        int num4 = drawContext.VerticalBorder + j * num2 + point.Y;
                        g.FillRectangle(brush, num3, num4, num, num2);
                        g.DrawRectangle(pen, num3, num4, num, num2);
                        if (showTemp)
                        {
                            Brush brush2 = new SolidBrush(this.TEMP_TEXT_COLOR);
                            g.DrawString(string.Format("{0,5:0.00}", sensor), font, brush2, (float)(num3 + num / 2), (float)(num4 + num2 / 2), stringFormat);
                        }
                    }
                }
            }
        }

        private float tempToTemp(float temp, float precision, MeasureModel measureModel)
        {
            float num = measureModel.MinTemp + (float)((int)((temp - measureModel.MinTemp) / precision)) * precision;
            if (num < measureModel.MinTemp)
            {
                num = measureModel.MinTemp;
            }
            if (num > measureModel.MaxTemp)
            {
                num = measureModel.MaxTemp;
            }
            return num;
        }

        private Color tempToColor(float temp, float minTemp, float maxTemp)
        {
            float num = (maxTemp - minTemp) / 3f;
            float num2 = minTemp + num;
            float num3 = num2 + num;
            int red = 0;
            int num4 = 0;
            int blue = 0;
            if (temp >= minTemp && temp < num2)
            {
                red = 0;
                num4 = (int)(255f * (temp - minTemp) / num);
                blue = 255;
            }
            if (temp >= num2 && temp < num3)
            {
                if (temp < num2 + (num3 - num2) / 2f)
                {
                    red = 0;
                    num4 = (int)(255f * (temp - num2) * 2f / num);
                    blue = 255 - num4;
                }
                else
                {
                    red = (int)(255f * (temp - (num2 + (num3 - num2) / 2f)) * 2f / num);
                    num4 = 255;
                    blue = 0;
                }
            }
            if (temp >= num3 && temp <= maxTemp)
            {
                red = 255;
                num4 = 255 - (int)(255f * (temp - num3) / num);
                blue = 0;
            }
            return Color.FromArgb(red, num4, blue);
        }

        private void drawActiveCell(Graphics g, MeasureModel measureModel, MeasureControlDrawContext drawContext, Point cell)
        {
            Pen pen = new Pen(this.ACTIVE_CELL_BORDER_COLOR, 2f);
            if (measureModel.isCellCanBeActive(cell))
            {
                Point point = this.gridToPoint(cell, drawContext);
                g.DrawRectangle(pen, drawContext.HorizontalBorder + point.X + 1, drawContext.VerticalBorder + point.Y + 1, drawContext.CellSize.Width - 2, drawContext.CellSize.Height - 2);
            }
        }

        private void drawTransparentImage(Graphics g, DrawContext drawContext, Image image, float opacity, int start, int end, bool drawLabel)
        {
            float[][] array = new float[5][];
            float[][] arg_1F_0 = array;
            int arg_1F_1 = 0;
            float[] array2 = new float[5];
            array2[0] = 1f;
            arg_1F_0[arg_1F_1] = array2;
            float[][] arg_36_0 = array;
            int arg_36_1 = 1;
            array2 = new float[5];
            array2[1] = 1f;
            arg_36_0[arg_36_1] = array2;
            float[][] arg_4D_0 = array;
            int arg_4D_1 = 2;
            array2 = new float[5];
            array2[2] = 1f;
            arg_4D_0[arg_4D_1] = array2;
            float[][] arg_61_0 = array;
            int arg_61_1 = 3;
            array2 = new float[5];
            array2[3] = opacity;
            arg_61_0[arg_61_1] = array2;
            array[4] = new float[]
			{
				0f,
				0f,
				0f,
				0f,
				1f
			};
            float[][] newColorMatrix = array;
            ColorMatrix newColorMatrix2 = new ColorMatrix(newColorMatrix);
            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(newColorMatrix2, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            int num = end - start + 1;
            if (num < 5)
            {
                num = 5;
            }
            int num2 = drawContext.Bounds.Height / num;
            Font font = new Font(this.LABEL_FONT_FAMILY, 6f);
            for (int i = 0; i < num; i++)
            {
                g.DrawImage(image, new Rectangle(drawContext.Bounds.Width / 2 - image.Width / 2, i * num2, image.Width, num2), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributes);
                if (drawLabel)
                {
                    g.DrawString(MeasureModel.VertebraLevels[start + i], font, new SolidBrush(this.BORDER_COLOR), new PointF((float)(drawContext.Bounds.Width - drawContext.HorizontalBorder), (float)(i * num2)));
                }
            }
        }
        private Point gridToPoint(Point grid, MeasureControlDrawContext drawContext)
        {
            int num = grid.X;
            int num2 = grid.Y - 1;
            if (num > 0)
            {
                num--;
            }
            int x = (drawContext.DrawGridSize.Width / 2 + num) * drawContext.CellSize.Width;
            int y = num2 * drawContext.CellSize.Height;
            return new Point(x, y);
        }
        private Point pointToGrid(Point point, MeasureControlDrawContext drawContext)
        {
            int num = point.X / drawContext.CellSize.Width;
            int num2 = point.Y / drawContext.CellSize.Height;
            int num3 = num - drawContext.DrawGridSize.Width / 2;
            if (num3 >= 0)
            {
                num3++;
            }
            int y = num2 + 1;
            return new Point(num3, y);
        }

        private Point pointToSensor(Point point, MeasureControlDrawContext drawContext)
        {
            Point grid = this.pointToGrid(point, drawContext);
            Point point2 = this.gridToPoint(grid, drawContext);
            int x = (point.X - point2.X) / (drawContext.CellSize.Width / com.medical.termo.data.Settings.Current.SensorInLine);
            int y = (point.Y - point2.Y) / (drawContext.CellSize.Height / com.medical.termo.data.Settings.Current.SensorInColumn);
            return new Point(x, y);
        }
        private string getAndCreatePath(Project project, string ext)
        {
            DateTime now = DateTime.Now;
            string workDirectory = com.medical.termo.data.Settings.Current.WorkDirectory;
            if (!Directory.Exists(workDirectory))
            {
                Directory.CreateDirectory(workDirectory);
            }
            string text = workDirectory + com.medical.termo.data.Settings.Current.Doctor.Hospital;
            if (!Directory.Exists(text))
            {
                Directory.CreateDirectory(text);
            }
            string text2 = text + "\\" + string.Format("{0:yyyy-MM-dd}", now);
            if (!Directory.Exists(text2))
            {
                Directory.CreateDirectory(text2);
            }
            string str = string.Concat(new string[]
			{
				project.Patient.SecondName,
				"-",
				project.Patient.FirstName,
				"-",
				project.Patient.MiddleName,
				"-",
				string.Format("{0:yyyy-MM-dd-HH-mm-ss}", now),
				".",
				ext
			});
            return text2 + "\\" + str;
        }









        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void _1_Load(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void exportButton_Click(object sender, EventArgs e)
        {

        }

        private void printButton_Click(object sender, EventArgs e)
        {

        }

        private void saveButton_Click(object sender, EventArgs e)
        {

        }

        private void workPanelTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void plotPanel_MouseClick(object sender, MouseEventArgs e)
        {
            Control control = sender as Control;
            Size size = control.Size;
            MeasureControlDrawContext drawContext = this.getDrawContext(this.measureModel, size, 15);
            Point point = this.pointToGrid(new Point(e.X - drawContext.HorizontalBorder, e.Y - drawContext.VerticalBorder), drawContext);
            if (this.measureModel.isCellCanBeActive(point))
            {
                this.activeCell = point;
                this.Refresh();
            }
        }

        private void plotPanel_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                Control control = sender as Control;
                Size size = control.Size;
                MeasureControlDrawContext drawContext = this.getDrawContext(this.measureModel, size, 15);
                Point point = new Point(e.X - drawContext.HorizontalBorder, e.Y - drawContext.VerticalBorder);
                Point point2 = this.pointToGrid(point, drawContext);
                Measure item = new Measure(point2.X, point2.Y);
                List<Measure> measures = this.measureModel.Measures;
                int num = measures.IndexOf(item);
                if (num >= 0)
                {
                    Point point3 = this.pointToSensor(point, drawContext);
                    float sensor = measures[num].getSensor(point3.X, point3.Y);
                    this.tempLabel.Text = string.Format("{0,5:0.00}", sensor) + '°';
                }
                else
                {
                    this.tempLabel.Text = string.Empty;
                }
            }
            catch (Exception)
            {

            }
        }

        private void upperLevelComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            this.plotPanel.Refresh();
        }

        private void gridWidthNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numericUpDown = sender as NumericUpDown;
            this.measureModel.MeasuresInDirection = new Size((int)numericUpDown.Value, this.measureModel.MeasuresInDirection.Height);
            this.activeCell = new Point(1, 1);
            this.measureModel.clearMeasures();
            this.Refresh();
        }

        private void gridHeightNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numericUpDown = sender as NumericUpDown;
            this.measureModel.MeasuresInDirection = new Size(this.measureModel.MeasuresInDirection.Width, (int)numericUpDown.Value);
            this.activeCell = new Point(1, 1);
            this.measureModel.clearMeasures();
            this.Refresh();
        }

        private void saveButton_Click_1(object sender, EventArgs e)
        {
            List<Measure> measures = this.measureModel.Measures;
            TermoSurvay surv = (TermoSurvay)project.Survays[0];
            surv.Data = measures;
            surv.Cols = (int)gridHeightNumericUpDown.Value;
            surv.Rows = (int)gridWidthNumericUpDown.Value;
            this.project.FinalDiagnosis = this.finalDiagnosisTextBox.Text;
            string andCreatePath = this.getAndCreatePath(this.project, "xml");
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Project), new Type[]
			{
				typeof(TermoSurvay)
			});
            StreamWriter streamWriter = new StreamWriter(andCreatePath);
            xmlSerializer.Serialize(streamWriter, this.project);
            streamWriter.Close();
            MessageBox.Show(this, "Файл успешно сохранен в папке " + Path.GetDirectoryName(andCreatePath), "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void printButton_Click_1(object sender, EventArgs e)
        {
            this.project.FinalDiagnosis = this.finalDiagnosisTextBox.Text;
            PrintDialog printDialog = new PrintDialog();
            PrintDocument printDocument = new PrintDocument();
            printDocument.PrintPage += new PrintPageEventHandler(this.printDocument_PrintPage);
            printDialog.Document = printDocument;
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                printDocument.Print();
            }
        }

        private void exportButton_Click_1(object sender, EventArgs e)
        {
            this.project.FinalDiagnosis = this.finalDiagnosisTextBox.Text;
            string andCreatePath = this.getAndCreatePath(this.project, "xls");
            ExportReportEngine exportReportEngine = new ExportReportEngine();
            exportReportEngine.export(this.project, andCreatePath);
            MessageBox.Show(this, "Файл успешно экспортирован в папку " + Path.GetDirectoryName(andCreatePath), "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            if (!this.lastPage)
            {
                PrintReportEngine printReportEngine = new PrintReportEngine(this.project);
                printReportEngine.printFirstPage(e.Graphics, e.MarginBounds);
                e.HasMorePages = true;
            }
            else
            {
                PrintReportEngine printReportEngine = new PrintReportEngine(this.project);
                printReportEngine.printSecondPage(e.Graphics, e.PageBounds);
                e.HasMorePages = false;
            }
            this.lastPage = !this.lastPage;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.prevControl();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.nextControl();
        }

        private void workPanelTabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {

        }

        private void workPanelTabControl_Selected(object sender, TabControlEventArgs e)
        {
            if (this.workPanelTabControl.SelectedIndex != this.currentControl)
            {
                this.workPanelTabControl.SelectedIndex = this.currentControl;
            }
        }
    }
}
