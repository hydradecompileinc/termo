using com.medical.termo.data;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace com.medical.termo.engine
{
	public class DrawReportEngine : BaseReportEngine
	{
		private class DrawContext
		{
			private Graphics graphics;

			private Font font;

			private Brush brush;

			public Graphics Graphics
			{
				get
				{
					return this.graphics;
				}
				set
				{
					this.graphics = value;
				}
			}

			public Font Font
			{
				get
				{
					return this.font;
				}
				set
				{
					this.font = value;
				}
			}

			public Brush Brush
			{
				get
				{
					return this.brush;
				}
				set
				{
					this.brush = value;
				}
			}

			public DrawContext(Graphics graphics, Font font, Brush brush)
			{
				this.graphics = graphics;
				this.font = font;
				this.brush = brush;
			}
		}

		private class TableDrawContext : DrawReportEngine.DrawContext
		{
			private Pen pen;

			private Size bounds;

			private int border;

			public Pen Pen
			{
				get
				{
					return this.pen;
				}
				set
				{
					this.pen = value;
				}
			}

			public Size Bounds
			{
				get
				{
					return this.bounds;
				}
				set
				{
					this.bounds = value;
				}
			}

			public int Border
			{
				get
				{
					return this.border;
				}
				set
				{
					this.border = value;
				}
			}

			public TableDrawContext(Graphics graphics, Font font, Brush brush, Pen pen, Size bounds, int border) : base(graphics, font, brush)
			{
				this.pen = pen;
				this.bounds = bounds;
				this.border = border;
			}
		}

		private const int TEXT_FONT_SIZE = 10;

		private const int MARGINE_TOP_BOTTOM = 30;

		private const int MARGINE_LEFT_RIGHT = 30;

		private Color TEXT_COLOR = Color.Black;

		private Color PEN_COLOR = Color.Black;

		private TermoReport report;

		public DrawReportEngine(TermoReport report)
		{
			this.report = report;
		}

		public PointF drawReport(Graphics g, int width)
		{
			Pen pen = new Pen(this.PEN_COLOR);
			Brush brush = new SolidBrush(this.TEXT_COLOR);
			Font font = new Font(FontFamily.GenericSansSerif, 10f);
			DrawReportEngine.DrawContext drawContext = new DrawReportEngine.DrawContext(g, font, brush);
			DrawReportEngine.TableDrawContext drawContext2 = new DrawReportEngine.TableDrawContext(g, font, brush, pen, new Size(width, 0), 30);
			StringFormat stringFormat = new StringFormat();
			stringFormat.Alignment = StringAlignment.Near;
			stringFormat.LineAlignment = StringAlignment.Near;
			float x = 30f;
			float y = 30f;
			PointF pointF = this.drawOneLine(drawContext, x, this.drawOneLine(drawContext, x, this.drawOneLine(drawContext, x, this.drawOneLine(drawContext, x, this.drawOneLine(drawContext, x, y, stringFormat, this.DELTA_TEMPERATURE_LABEL, this.report.DeltaTemp, "°C").Y, stringFormat, this.AVERAGE_TEMPERATURE_LEFT_LABEL, this.report.AverageLeftTemp, "°C").Y, stringFormat, this.AVERAGE_TEMPERATURE_RIGHT_LABEL, this.report.AverageRightTemp, "°C").Y, stringFormat, this.AVERAGE_TEMPERATURE_ASYMMETRY_LABEL, this.report.AverageAsymmetry, "°C").Y + 30f, stringFormat, this.AVERAGE_TEMPERATURE_AT_POSITION_HEADER + "°C");
			pointF = this.drawOneLine(drawContext, x, this.drawTable(drawContext2, pointF, 3, 9, this.report.ListOfTemp).Y + 30f, stringFormat, this.DELTA_BETWEEN_AVERAGE_TEMPERATRE_AT_SYMMETRIC_POSITION_HEADER + "°C");
			pointF = this.drawOneLine(drawContext, x, this.drawTable(drawContext2, pointF, 2, 9, this.report.ListOfDeltaBetweenSymmetricPosition).Y + 30f, stringFormat, this.DELTA_BETWEEN_POSITION_TEMPERATRE_AND_AVERAGE_HEADER + "°C");
			pointF = this.drawOneLine(drawContext, x, this.drawTable(drawContext2, pointF, 3, 9, this.report.ListOfDeltaBetweenPositionAndAverage).Y + 30f, stringFormat, this.DELTA_BETWEEN_AVERAGE_TEMPERATRE_AT_SYMMETRIC_SENSOR_HEADER + "°C");
			pointF = this.drawOneLine(drawContext, x, this.drawTable(drawContext2, pointF, 2, 9, this.report.ListOfDeltaBetweenSymmetricSensor).Y + 30f, stringFormat, this.DELTA_BETWEEN_SENSOR_TEMPERATRE_AND_AVERAGE_HEADER + "°C");
			pointF = this.drawOneLine(drawContext, x, this.drawTable(drawContext2, pointF, 3, 9, this.report.ListOfDeltaBetweenSensorAndAverage).Y + 30f, stringFormat, this.DELTA_BETWEEN_SENSOR_TEMPERATRE_AND_POSITION_HEADER + "°C");
			pointF = this.drawOneLine(drawContext, x, this.drawTable(drawContext2, pointF, 3, 9, this.report.ListOfDeltaBetweenSensorAndPosition).Y + 30f, stringFormat, this.HYPERTERMIA_HEADER);
			PointF pointF2 = pointF;
			pointF = this.drawOneLine(drawContext, this.drawOneLine(drawContext, x, pointF.Y, stringFormat, this.MAX_UPPER_ABOVE_AVERAGE_LEFT, this.report.MaxLocalUpperLeft, "°C").X, pointF2.Y, stringFormat, " " + this.SQUARE + ":" + base.getLabelByCellCoordinate(this.report.CellMaxLocalUpperLeft));
			pointF2 = pointF;
			pointF = this.drawOneLine(drawContext, this.drawOneLine(drawContext, x, pointF.Y, stringFormat, this.MAX_UPPER_ABOVE_AVERAGE_RIGHT, this.report.MaxLocalUpperRight, "°C").X, pointF2.Y, stringFormat, " " + this.SQUARE + ":" + base.getLabelByCellCoordinate(this.report.CellMaxLocalUpperRight));
			pointF2 = pointF;
			return this.drawOneLine(drawContext, this.drawOneLine(drawContext, x, pointF.Y, stringFormat, this.MAX_LOCAL_ASYMMETRY, this.report.MaxLocalAsymmetry, "°C").X, pointF2.Y, stringFormat, " " + this.SQUARE + ":" + base.getLabelByCellCoordinate(this.report.CellLocalAsymmetry));
		}

		private PointF drawOneLine(DrawReportEngine.DrawContext drawContext, float x, float y, StringFormat format, string text, float value, string suffix)
		{
			string text2 = string.Format("{0} = {1}" + suffix, text, base.formatValue(value));
			return this.drawOneLine(drawContext, x, y, format, text2);
		}

		private PointF drawOneLine(DrawReportEngine.DrawContext drawContext, float x, float y, StringFormat format, float value)
		{
			string text = base.formatValue(value);
			return this.drawOneLine(drawContext, x, y, format, text);
		}

		private PointF drawOneLine(DrawReportEngine.DrawContext drawContext, float x, float y, StringFormat format, string text)
		{
			PointF point = new PointF(x, y);
			drawContext.Graphics.DrawString(text, drawContext.Font, drawContext.Brush, point, format);
			SizeF sizeF = drawContext.Graphics.MeasureString(text, drawContext.Font);
			return new PointF(point.X + sizeF.Width, point.Y + sizeF.Height);
		}

		private PointF drawTable(DrawReportEngine.TableDrawContext drawContext, PointF point, int rowCount, int columnCount, List<TermoReportItem> listOfItem)
		{
			string text = "Слева";
			string text2 = "Справа";
			StringFormat stringFormat = new StringFormat();
			stringFormat.Alignment = StringAlignment.Far;
			stringFormat.LineAlignment = StringAlignment.Near;
			StringFormat stringFormat2 = new StringFormat();
			stringFormat2.Alignment = StringAlignment.Center;
			stringFormat2.LineAlignment = StringAlignment.Center;
			float num = (float)((drawContext.Bounds.Width - drawContext.Border * 2) / columnCount);
			float num2 = drawContext.Graphics.MeasureString("0", drawContext.Font).Height + 4f;
			DrawReportEngine.DrawContext drawContext2 = new DrawReportEngine.DrawContext(drawContext.Graphics, drawContext.Font, drawContext.Brush);
			for (int i = 0; i < rowCount; i++)
			{
				for (int j = 0; j < columnCount; j++)
				{
					float num3 = (float)drawContext.Border + (float)j * num;
					float num4 = point.Y + (float)i * num2;
					drawContext.Graphics.DrawRectangle(drawContext.Pen, num3, num4, num, num2);
					if (i == 0 && j != 0)
					{
						this.drawOneLine(drawContext2, num3 + num / 2f, point.Y + 2f + num2 / 2f, stringFormat2, base.getLabelByCellIndex(j));
					}
					if (rowCount > 2 && j == 0)
					{
						string text3 = string.Empty;
						if (i == 1)
						{
							text3 = text2;
						}
						if (i == 2)
						{
							text3 = text;
						}
						this.drawOneLine(drawContext2, num3 + num - 2f, num4 + 2f, stringFormat, text3);
					}
				}
			}
			foreach (TermoReportItem current in listOfItem)
			{
				float num5 = point.Y + num2 + 2f;
				if (rowCount > 2 && current.GridX < 0)
				{
					num5 += num2;
				}
				float num6 = 30f + num + num * (float)base.getIndexByCellCoordinate(new Point(current.GridX, current.GridY));
				this.drawOneLine(drawContext2, num6 - 2f, num5, stringFormat, current.ItemValue);
			}
			return new PointF(point.X, point.Y + (float)rowCount * num2);
		}
	}
}
