using com.medical.common.data;
using com.medical.termo.data;
using System;
using System.Drawing;

namespace com.medical.termo.engine
{
	public class PrintReportEngine : BaseReportEngine
	{
		private const int HEADER_FONT_SIZE = 14;

		private const int TEXT_FONT_SIZE = 12;

		private const int INTERVAL = 20;

		private Color textColor = Color.Black;

		private Project project;

		public PrintReportEngine(Project project)
		{
			this.project = project;
		}

		public void printFirstPage(Graphics g, Rectangle bounds)
		{
			Font font = new Font(new FontFamily("Times New Roman"), 12f);
			Font font2 = new Font(new FontFamily("Times New Roman"), 12f, FontStyle.Bold);
			Font font3 = new Font(new FontFamily("Times New Roman"), 14f, FontStyle.Bold);
			Brush brush = new SolidBrush(this.textColor);
			StringFormat stringFormat = new StringFormat();
			stringFormat.Alignment = StringAlignment.Center;
			stringFormat.LineAlignment = StringAlignment.Near;
			StringFormat stringFormat2 = new StringFormat();
			stringFormat2.Alignment = StringAlignment.Near;
			stringFormat2.LineAlignment = StringAlignment.Near;
			float num = (float)bounds.Left;
			float num2 = (float)bounds.Top;
			TermoSurvay termoSurvay = (TermoSurvay)this.project.Survays[0];
			string text = this.HEADER;
			g.DrawString(text, font3, brush, num + (float)(bounds.Width / 2), num2, stringFormat);
			num2 += g.MeasureString(text, font3).Height;
			num2 += 20f;
			text = this.DATE;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			float num3 = num + g.MeasureString(text, font2).Width;
			text = " " + termoSurvay.Date.ToString();
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.PATIENT;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = string.Concat(new string[]
			{
				" ",
				this.project.Patient.SecondName,
				" ",
				this.project.Patient.FirstName,
				" ",
				this.project.Patient.MiddleName
			});
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.DATE_OF_BIRTH;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = " " + string.Format("{0:dd/MM/yyyy}", this.project.Patient.DateOfBirth);
			g.DrawString(text, font, brush, num3, num2);
			num3 += g.MeasureString(text, font).Width;
			text = this.AGE;
			g.DrawString(text, font2, brush, num3, num2, stringFormat2);
			num3 += g.MeasureString(text, font2).Width;
			text = " " + this.project.Patient.Age.ToString();
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.SEX;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = " " + SexToStringConverter.toString(this.project.Patient.Sex);
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.WEIGHT;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = " " + this.project.Patient.Weight.ToString() + " кг";
			g.DrawString(text, font, brush, num3, num2);
			num3 += g.MeasureString(text, font2).Width;
			text = this.HEIGHT;
			g.DrawString(text, font2, brush, num3, num2);
			num3 += g.MeasureString(text, font2).Width;
			text = " " + this.project.Patient.Height.ToString() + " см";
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.ENVIRONMENT_TEMP;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = " " + ((TermoSurvay)this.project.Survays[0]).EnvironmentTemp.ToString() + "°C";
			g.DrawString(text, font, brush, num3, num2);
			num3 += g.MeasureString(text, font).Width;
			text = this.LEFT_AXILLARY_TEMP;
			g.DrawString(text, font2, brush, num3, num2, stringFormat2);
			num3 += g.MeasureString(text, font2).Width;
			text = " " + ((TermoSurvay)this.project.Survays[0]).LeftAxillaryTemp.ToString() + "°C";
			g.DrawString(text, font, brush, num3, num2);
			num3 += g.MeasureString(text, font).Width;
			text = this.RIGHT_AXILLARY_TEMP;
			g.DrawString(text, font2, brush, num3, num2, stringFormat2);
			num3 += g.MeasureString(text, font2).Width;
			text = " " + ((TermoSurvay)this.project.Survays[0]).RightAxillaryTemp.ToString() + "°C";
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.PRELIMINARY_DIAGNOSIS;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = " " + this.project.PreliminaryDiagnosis;
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.FINAL_DIAGNOSIS;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = " " + this.project.FinalDiagnosis;
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.HOSPITAL;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = " " + termoSurvay.Doctor.Hospital;
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			text = this.DOCTOR;
			g.DrawString(text, font2, brush, num, num2, stringFormat2);
			num3 = num + g.MeasureString(text, font2).Width;
			text = string.Concat(new string[]
			{
				" ",
				termoSurvay.Doctor.SecondName,
				" ",
				termoSurvay.Doctor.FirstName,
				" ",
				termoSurvay.Doctor.MiddleName
			});
			g.DrawString(text, font, brush, num3, num2);
			num2 += g.MeasureString(text, font3).Height;
			num2 += 20f;
			text = this.TERMOGRAM_HEADER;
			g.DrawString(text, font3, brush, num + (float)(bounds.Width / 2), num2, stringFormat);
			num2 += g.MeasureString(text, font3).Height;
			num2 += 20f;
			float num4 = (float)(bounds.Width - 20);
			float num5 = (float)(bounds.Bottom - 20) - num2;
			if (num5 < num4)
			{
				num4 = num5;
			}
			if (num5 > num4)
			{
				num5 = num4;
			}
			g.DrawImage(termoSurvay.Termogram, num + (float)(bounds.Width / 2) - num4 / 2f, num2, num4, num5);
			num2 = (float)bounds.Bottom;
			text = "Стр. 1 из 2";
			g.DrawString(text, font, brush, num + (float)(bounds.Width / 2), num2, stringFormat);
		}

		public void printSecondPage(Graphics g, Rectangle bounds)
		{
			Font font = new Font(new FontFamily("Times New Roman"), 12f);
			Font font2 = new Font(new FontFamily("Times New Roman"), 14f, FontStyle.Bold);
			Brush brush = new SolidBrush(this.textColor);
			StringFormat stringFormat = new StringFormat();
			stringFormat.Alignment = StringAlignment.Center;
			stringFormat.LineAlignment = StringAlignment.Near;
			TermoSurvay termoSurvay = (TermoSurvay)this.project.Survays[0];
			DrawReportEngine drawReportEngine = new DrawReportEngine(termoSurvay.TermoReport);
			drawReportEngine.drawReport(g, bounds.Width - 40);
			g.DrawString("Стр. 2 из 2", font, brush, (float)(bounds.Left + bounds.Width / 2), (float)(bounds.Bottom - 60), stringFormat);
		}
	}
}
