using System;
using System.Drawing;

namespace com.medical.termo.engine
{
	public abstract class BaseReportEngine
	{
		protected const string DEGREE = "°C";

		protected const int TABLE_COLUMN_COUNT = 9;

		protected const int TABLE_ROW_SMALL = 2;

		protected const int TABLE_ROW_BIG = 3;

		protected string HEADER = "ПРОТОКОЛ ТЕРМОГРАФИЧЕСКОГО ИССЛЕДОВАНИЯ";

		protected string DATE = "Дата:";

		protected string PATIENT = "Пациент:";

		protected string DATE_OF_BIRTH = "Дата рождения:";

		protected string AGE = "Возраст:";

		protected string SEX = "Пол:";

		protected string WEIGHT = "Вес:";

		protected string HEIGHT = "Рост:";

		protected string ENVIRONMENT_TEMP = "Т-ра окр. среды:";

		protected string LEFT_AXILLARY_TEMP = "Т-ра аксиллярно слева:";

		protected string RIGHT_AXILLARY_TEMP = "Т-ра аксиллярно справа:";

		protected string HOSPITAL = "Лечебное учреждение:";

		protected string PRELIMINARY_DIAGNOSIS = "Предварительный диагноз:";

		protected string FINAL_DIAGNOSIS = "Окончательный диагноз";

		protected string DOCTOR = "Исследование провел:";

		protected string TERMOGRAM_HEADER = "ТЕРМОГРАММА";

		protected string DELTA_TEMPERATURE_LABEL = "Наблюдаемый в термограмме разброс температур";

		protected string AVERAGE_TEMPERATURE_LEFT_LABEL = "Средняя температура с левой стороны";

		protected string AVERAGE_TEMPERATURE_RIGHT_LABEL = "Средняя температура с правой стороны";

		protected string AVERAGE_TEMPERATURE_ASYMMETRY_LABEL = "Средняя асимметрия температур";

		protected string AVERAGE_TEMPERATURE_AT_POSITION_HEADER = "Средняя температура квадрантов, ";

		protected string DELTA_BETWEEN_POSITION_TEMPERATRE_AND_AVERAGE_HEADER = "Разность между средними т-рами квадрантов и средней т-рой стороны, ";

		protected string DELTA_BETWEEN_SENSOR_TEMPERATRE_AND_AVERAGE_HEADER = "Макс. разность между т-рой датчика в квадранте и средней т-рой стороны, ";

		protected string DELTA_BETWEEN_SENSOR_TEMPERATRE_AND_POSITION_HEADER = "Макс. разность между т-рой датчика в квадранте и средней т-рой квадранта, ";

		protected string DELTA_BETWEEN_AVERAGE_TEMPERATRE_AT_SYMMETRIC_POSITION_HEADER = "Разность между средними т-рами симметричных квадрантов (модуль), ";

		protected string DELTA_BETWEEN_AVERAGE_TEMPERATRE_AT_SYMMETRIC_SENSOR_HEADER = "Макс. разность показаний симметричных датчиков (модуль), ";

		protected string HYPERTERMIA_HEADER = "Зоны гипертермии";

		protected string MAX_UPPER_ABOVE_AVERAGE_LEFT = "Макс. превышение над средней т-рой слева";

		protected string MAX_UPPER_ABOVE_AVERAGE_RIGHT = "Макс. превышение над средней т-рой справа";

		protected string MAX_LOCAL_ASYMMETRY = "Макс. локальная асимметрия";

		protected string SQUARE = "Квадрант";

		protected string formatValue(float value)
		{
			string result = string.Empty;
			if (!float.IsNaN(value) && !float.IsInfinity(value))
			{
				result = string.Format("{0,5:0.00}", value);
			}
			return result;
		}

		protected int getIndexByCellCoordinate(Point coordinate)
		{
			int result = -1;
			if (coordinate.X != 0 && coordinate.Y != 0)
			{
				result = (Math.Abs(coordinate.X) - 1) * 4 + coordinate.Y;
			}
			return result;
		}

		protected string getLabelByCellIndex(int index)
		{
			string[] array = new string[]
			{
				"A",
				"B",
				"C",
				"D"
			};
			int num = (index - 1) % array.Length;
			int num2 = (index - 1) / array.Length + 1;
			return array[num] + num2.ToString();
		}

		protected string getLabelByCellCoordinate(Point coordinate)
		{
			string text = string.Empty;
			string str = "Слева";
			string str2 = "Справа";
			int indexByCellCoordinate = this.getIndexByCellCoordinate(coordinate);
			if (indexByCellCoordinate != -1)
			{
				text = this.getLabelByCellIndex(indexByCellCoordinate);
				text += " ";
				if (coordinate.X < 0)
				{
					text += str;
				}
				else
				{
					text += str2;
				}
			}
			return text;
		}
	}
}
