using System;
using System.Drawing;

namespace com.medical.termo.engine
{
	public class TermogramEngine
	{
		private const int GRAPH_BORDER = 20;

		private const float TERMO_PERCENT = 0.85f;

		private Bitmap Graph;

		private Bitmap Termogram;

		private Bitmap Scale;

		public int GraphBorder
		{
			get
			{
				return 20;
			}
		}

		public TermogramEngine(Size size)
		{
			this.Graph = new Bitmap(size.Width, size.Height);
			int num = (int)((float)this.Graph.Width * 0.85f);
			int num2 = this.Graph.Height;
			if (num < num2)
			{
				num2 = num;
			}
			if (num2 < num)
			{
				num = num2;
			}
			this.Termogram = new Bitmap(num, num2);
			int width = this.Graph.Width - num;
			int height = num2 - 40;
			this.Scale = new Bitmap(width, height);
		}

		public Graphics getTermoGraphics()
		{
			Graphics graphics = Graphics.FromImage(this.Termogram);
			graphics.Clear(Color.White);
			return graphics;
		}

		public Graphics getScaleGraphics()
		{
			Graphics graphics = Graphics.FromImage(this.Scale);
			graphics.Clear(Color.White);
			return graphics;
		}

		public Size getTermoSize()
		{
			return this.Termogram.Size;
		}

		public Size getScaleSize()
		{
			return this.Scale.Size;
		}

		public Bitmap getGraph()
		{
			Graphics graphics = Graphics.FromImage(this.Graph);
			graphics.Clear(Color.White);
			graphics.DrawImage(this.Scale, 0, 20);
			graphics.DrawImage(this.Termogram, this.Scale.Width, 0);
			return this.Graph;
		}
	}
}
