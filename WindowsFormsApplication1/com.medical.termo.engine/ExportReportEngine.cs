using com.medical.common.data;
using com.medical.termo.data;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace com.medical.termo.engine
{
	public class ExportReportEngine : BaseReportEngine
	{
		public void export(Project project, string pathToExport)
		{
			TermoSurvay termoSurvay = (TermoSurvay)project.Survays[0];
			HSSFWorkbook hSSFWorkbook = new HSSFWorkbook();
			DocumentSummaryInformation documentSummaryInformation = PropertySetFactory.CreateDocumentSummaryInformation();
			documentSummaryInformation.Company = "Victor Babkov";
			hSSFWorkbook.DocumentSummaryInformation = documentSummaryInformation;
			SummaryInformation summaryInformation = PropertySetFactory.CreateSummaryInformation();
			summaryInformation.Subject = "Termo report";
			hSSFWorkbook.SummaryInformation = summaryInformation;
			Sheet sheet = hSSFWorkbook.CreateSheet("Термограмма");
			Sheet sheet2 = hSSFWorkbook.CreateSheet("Расчет");
			sheet.CreateRow(1).CreateCell(3).SetCellValue(this.HEADER);
			sheet.CreateRow(3).CreateCell(0).SetCellValue(this.DATE + " " + termoSurvay.Date.ToString());
			sheet.CreateRow(5).CreateCell(0).SetCellValue(string.Concat(new string[]
			{
				this.PATIENT,
				" ",
				project.Patient.SecondName,
				" ",
				project.Patient.FirstName,
				" ",
				project.Patient.MiddleName
			}));
			sheet.CreateRow(7).CreateCell(0).SetCellValue(string.Concat(new string[]
			{
				this.DATE_OF_BIRTH,
				" ",
				string.Format("{0:dd/MM/yyyy}", project.Patient.DateOfBirth),
				" ",
				this.AGE,
				" ",
				project.Patient.Age.ToString()
			}));
			sheet.CreateRow(9).CreateCell(0).SetCellValue(this.SEX + " " + SexToStringConverter.toString(project.Patient.Sex));
			sheet.CreateRow(11).CreateCell(0).SetCellValue(string.Concat(new string[]
			{
				this.WEIGHT,
				" ",
				project.Patient.Weight.ToString(),
				" ",
				this.HEIGHT,
				" ",
				project.Patient.Height.ToString()
			}));
			sheet.CreateRow(13).CreateCell(0).SetCellValue(string.Concat(new string[]
			{
				this.ENVIRONMENT_TEMP,
				" ",
				termoSurvay.EnvironmentTemp.ToString(),
				"°C ",
				this.LEFT_AXILLARY_TEMP,
				" ",
				termoSurvay.LeftAxillaryTemp.ToString(),
				"°C ",
				this.RIGHT_AXILLARY_TEMP,
				" ",
				termoSurvay.RightAxillaryTemp.ToString(),
				"°C"
			}));
			sheet.CreateRow(15).CreateCell(0).SetCellValue(this.PRELIMINARY_DIAGNOSIS + " " + project.PreliminaryDiagnosis);
			sheet.CreateRow(17).CreateCell(0).SetCellValue(this.FINAL_DIAGNOSIS + " " + project.FinalDiagnosis);
			sheet.CreateRow(19).CreateCell(0).SetCellValue(this.HOSPITAL + " " + termoSurvay.Doctor.Hospital);
			sheet.CreateRow(21).CreateCell(0).SetCellValue(string.Concat(new string[]
			{
				this.DOCTOR,
				" ",
				termoSurvay.Doctor.SecondName,
				" ",
				termoSurvay.Doctor.FirstName,
				" ",
				termoSurvay.Doctor.MiddleName
			}));
			sheet.CreateRow(23).CreateCell(4).SetCellValue(this.TERMOGRAM_HEADER);
			Drawing drawing = sheet.CreateDrawingPatriarch();
			HSSFClientAnchor hSSFClientAnchor = new HSSFClientAnchor(0, 0, 0, 255, 2, 25, 2, 25);
			hSSFClientAnchor.AnchorType = 2;
			MemoryStream memoryStream = new MemoryStream();
			termoSurvay.Termogram.Save(memoryStream, ImageFormat.Png);
			int pictureIndex = hSSFWorkbook.AddPicture(memoryStream.ToArray(), PictureType.PNG);
			Picture picture = drawing.CreatePicture(hSSFClientAnchor, pictureIndex);
			picture.Resize();
			picture.LineStyle = HSSFShape.LINESTYLE_NONE;
			TermoReport termoReport = termoSurvay.TermoReport;
			sheet2.SetColumnWidth(0, 12800);
			sheet2.CreateRow(1).CreateCell(0).SetCellValue(this.DELTA_TEMPERATURE_LABEL + ", °C");
			sheet2.GetRow(1).CreateCell(1).SetCellValue(base.formatValue(termoReport.DeltaTemp));
			sheet2.CreateRow(3).CreateCell(0).SetCellValue(this.AVERAGE_TEMPERATURE_LEFT_LABEL + ", °C");
			sheet2.GetRow(3).CreateCell(1).SetCellValue(base.formatValue(termoReport.AverageLeftTemp));
			sheet2.CreateRow(5).CreateCell(0).SetCellValue(this.AVERAGE_TEMPERATURE_RIGHT_LABEL + ", °C");
			sheet2.GetRow(5).CreateCell(1).SetCellValue(base.formatValue(termoReport.AverageRightTemp));
			sheet2.CreateRow(7).CreateCell(0).SetCellValue(this.AVERAGE_TEMPERATURE_ASYMMETRY_LABEL + ", °C");
			sheet2.GetRow(7).CreateCell(1).SetCellValue(base.formatValue(termoReport.AverageAsymmetry));
			sheet2.CreateRow(9).CreateCell(1).SetCellValue(this.AVERAGE_TEMPERATURE_AT_POSITION_HEADER + "°C");
			this.drawTable(sheet2, 0, 10, 3, 9, termoReport.ListOfTemp);
			sheet2.CreateRow(14).CreateCell(1).SetCellValue(this.DELTA_BETWEEN_AVERAGE_TEMPERATRE_AT_SYMMETRIC_POSITION_HEADER + "°C");
			this.drawTable(sheet2, 0, 15, 2, 9, termoReport.ListOfDeltaBetweenSymmetricPosition);
			sheet2.CreateRow(18).CreateCell(1).SetCellValue(this.DELTA_BETWEEN_POSITION_TEMPERATRE_AND_AVERAGE_HEADER + "°C");
			this.drawTable(sheet2, 0, 19, 3, 9, termoReport.ListOfDeltaBetweenPositionAndAverage);
			sheet2.CreateRow(23).CreateCell(1).SetCellValue(this.DELTA_BETWEEN_AVERAGE_TEMPERATRE_AT_SYMMETRIC_SENSOR_HEADER + "°C");
			this.drawTable(sheet2, 0, 24, 2, 9, termoReport.ListOfDeltaBetweenSymmetricSensor);
			sheet2.CreateRow(27).CreateCell(1).SetCellValue(this.DELTA_BETWEEN_SENSOR_TEMPERATRE_AND_AVERAGE_HEADER + "°C");
			this.drawTable(sheet2, 0, 28, 3, 9, termoReport.ListOfDeltaBetweenSensorAndAverage);
			sheet2.CreateRow(32).CreateCell(1).SetCellValue(this.DELTA_BETWEEN_SENSOR_TEMPERATRE_AND_POSITION_HEADER + "°C");
			this.drawTable(sheet2, 0, 33, 3, 9, termoReport.ListOfDeltaBetweenSensorAndPosition);
			sheet2.CreateRow(37).CreateCell(0).SetCellValue(this.HYPERTERMIA_HEADER);
			sheet2.CreateRow(38).CreateCell(0).SetCellValue(this.MAX_UPPER_ABOVE_AVERAGE_LEFT + ", °C");
			sheet2.GetRow(38).CreateCell(1).SetCellValue(base.formatValue(termoReport.MaxLocalUpperLeft));
			sheet2.GetRow(38).CreateCell(3).SetCellValue(this.SQUARE);
			sheet2.GetRow(38).CreateCell(4).SetCellValue(base.getLabelByCellCoordinate(termoReport.CellMaxLocalUpperLeft));
			sheet2.CreateRow(39).CreateCell(0).SetCellValue(this.MAX_UPPER_ABOVE_AVERAGE_RIGHT + ", °C");
			sheet2.GetRow(39).CreateCell(1).SetCellValue(base.formatValue(termoReport.MaxLocalUpperRight));
			sheet2.GetRow(39).CreateCell(3).SetCellValue(this.SQUARE);
			sheet2.GetRow(39).CreateCell(4).SetCellValue(base.getLabelByCellCoordinate(termoReport.CellMaxLocalUpperRight));
			sheet2.CreateRow(40).CreateCell(0).SetCellValue(this.MAX_LOCAL_ASYMMETRY + ", °C");
			sheet2.GetRow(40).CreateCell(1).SetCellValue(base.formatValue(termoReport.MaxLocalAsymmetry));
			sheet2.GetRow(40).CreateCell(3).SetCellValue(this.SQUARE);
			sheet2.GetRow(40).CreateCell(4).SetCellValue(base.getLabelByCellCoordinate(termoReport.CellLocalAsymmetry));
			FileStream fileStream = new FileStream(pathToExport, FileMode.Create);
			hSSFWorkbook.Write(fileStream);
			fileStream.Close();
		}

		public void export(List<Project> listOfProject, string pathToExport)
		{
			string[] array = new string[]
			{
				"Дата",
				"ФИО",
				"Пол",
				"Дата рождения",
				"Возраст",
				"Вес, кг",
				"Рост, см",
				"ТОкрСр,°C",
				"ТАксЛ,°C",
				"ТАксП,°C",
				"Тип решетки",
				"TсрП,°C",
				"ТсрЛ,°C",
				"АссмСр,°C",
				"ПревМаксП,°C",
				"Квадрант",
				"ПревМаксЛ,°C",
				"Квадрант",
				"МаксАссм,°C",
				"Квадрант",
				"Предварительный диагноз",
				"Окончательный диагноз"
			};
			HSSFWorkbook hSSFWorkbook = new HSSFWorkbook();
			DocumentSummaryInformation documentSummaryInformation = PropertySetFactory.CreateDocumentSummaryInformation();
			documentSummaryInformation.Company = "Victor Babkov";
			hSSFWorkbook.DocumentSummaryInformation = documentSummaryInformation;
			SummaryInformation summaryInformation = PropertySetFactory.CreateSummaryInformation();
			summaryInformation.Subject = "Termo report";
			hSSFWorkbook.SummaryInformation = summaryInformation;
			Sheet sheet = hSSFWorkbook.CreateSheet("Общий отчет");
			int num = 1;
			sheet.CreateRow(num).CreateCell(0).SetCellValue("Условные обозначения");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[7]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Температура окружающей среды");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[8]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Температура аксиллярно слева");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[9]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Температура аксиллярно справа");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[11]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Средняя температура правой стороны");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[12]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Средняя температура левой стороны");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[13]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Средняя асимметрия");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[14]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Максимальное превышение средней температуры с правой стороны");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[16]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Максимальное превышение средней температуры с левой стороны");
			num++;
			sheet.CreateRow(num).CreateCell(0).SetCellValue(array[18]);
			sheet.GetRow(num).CreateCell(1).SetCellValue("Максимальная локальная асимметрия");
			num += 2;
			sheet.CreateRow(num);
			for (int i = 0; i < array.Length; i++)
			{
				sheet.GetRow(num).CreateCell(i).SetCellValue(array[i]);
			}
			sheet.SetColumnWidth(0, 5120);
			sheet.SetColumnWidth(1, 10240);
			sheet.SetColumnWidth(2, 3840);
			sheet.SetColumnWidth(3, 5120);
			sheet.SetColumnWidth(4, 3840);
			sheet.SetColumnWidth(5, 3840);
			sheet.SetColumnWidth(6, 3840);
			sheet.SetColumnWidth(7, 3840);
			sheet.SetColumnWidth(8, 3840);
			sheet.SetColumnWidth(9, 3840);
			sheet.SetColumnWidth(10, 3840);
			sheet.SetColumnWidth(11, 3840);
			sheet.SetColumnWidth(12, 3840);
			sheet.SetColumnWidth(13, 3840);
			sheet.SetColumnWidth(14, 3840);
			sheet.SetColumnWidth(15, 3840);
			sheet.SetColumnWidth(16, 3840);
			sheet.SetColumnWidth(17, 3840);
			sheet.SetColumnWidth(18, 3840);
			sheet.SetColumnWidth(19, 3840);
			sheet.SetColumnWidth(20, 10240);
			sheet.SetColumnWidth(21, 10240);
			num++;
			for (int i = 0; i < listOfProject.Count; i++)
			{
				Project project = listOfProject[i];
				TermoSurvay termoSurvay = (TermoSurvay)project.Survays[0];
				DateTime date = termoSurvay.Date;
				string cellValue = string.Concat(new string[]
				{
					project.Patient.SecondName,
					" ",
					project.Patient.FirstName,
					" ",
					project.Patient.MiddleName
				});
				string cellValue2 = SexToStringConverter.toString(project.Patient.Sex);
				string cellValue3 = string.Format("{0:dd/MM/yyyy}", project.Patient.DateOfBirth);
				string age = project.Patient.Age;
				float weight = project.Patient.Weight;
				int height = project.Patient.Height;
				float environmentTemp = termoSurvay.EnvironmentTemp;
				float leftAxillaryTemp = termoSurvay.LeftAxillaryTemp;
				float rightAxillaryTemp = termoSurvay.RightAxillaryTemp;
				string preliminaryDiagnosis = project.PreliminaryDiagnosis;
				string finalDiagnosis = project.FinalDiagnosis;
				string cellValue4 = termoSurvay.MeasuresInDirection.Width.ToString() + "x" + termoSurvay.MeasuresInDirection.Height;
				float averageRightTemp = termoSurvay.TermoReport.AverageRightTemp;
				float averageLeftTemp = termoSurvay.TermoReport.AverageLeftTemp;
				float averageAsymmetry = termoSurvay.TermoReport.AverageAsymmetry;
				float maxLocalUpperRight = termoSurvay.TermoReport.MaxLocalUpperRight;
				Point cellMaxLocalUpperRight = termoSurvay.TermoReport.CellMaxLocalUpperRight;
				float maxLocalUpperLeft = termoSurvay.TermoReport.MaxLocalUpperLeft;
				Point cellMaxLocalUpperLeft = termoSurvay.TermoReport.CellMaxLocalUpperLeft;
				float maxLocalAsymmetry = termoSurvay.TermoReport.MaxLocalAsymmetry;
				Point cellLocalAsymmetry = termoSurvay.TermoReport.CellLocalAsymmetry;
				sheet.CreateRow(num + i).CreateCell(0).SetCellValue(date.ToString());
				sheet.GetRow(num + i).CreateCell(1).SetCellValue(cellValue);
				sheet.GetRow(num + i).CreateCell(2).SetCellValue(cellValue2);
				sheet.GetRow(num + i).CreateCell(3).SetCellValue(cellValue3);
				sheet.GetRow(num + i).CreateCell(4).SetCellValue(age);
				sheet.GetRow(num + i).CreateCell(5).SetCellValue(base.formatValue(weight));
				sheet.GetRow(num + i).CreateCell(6).SetCellValue(base.formatValue((float)height));
				sheet.GetRow(num + i).CreateCell(7).SetCellValue(base.formatValue(environmentTemp));
				sheet.GetRow(num + i).CreateCell(8).SetCellValue(base.formatValue(leftAxillaryTemp));
				sheet.GetRow(num + i).CreateCell(9).SetCellValue(base.formatValue(rightAxillaryTemp));
				sheet.GetRow(num + i).CreateCell(10).SetCellValue(cellValue4);
				sheet.GetRow(num + i).CreateCell(11).SetCellValue(base.formatValue(averageRightTemp));
				sheet.GetRow(num + i).CreateCell(12).SetCellValue(base.formatValue(averageLeftTemp));
				sheet.GetRow(num + i).CreateCell(13).SetCellValue(base.formatValue(averageAsymmetry));
				sheet.GetRow(num + i).CreateCell(14).SetCellValue(base.formatValue(maxLocalUpperRight));
				sheet.GetRow(num + i).CreateCell(15).SetCellValue(base.getLabelByCellCoordinate(cellMaxLocalUpperRight));
				sheet.GetRow(num + i).CreateCell(16).SetCellValue(base.formatValue(maxLocalUpperLeft));
				sheet.GetRow(num + i).CreateCell(17).SetCellValue(base.getLabelByCellCoordinate(cellMaxLocalUpperLeft));
				sheet.GetRow(num + i).CreateCell(18).SetCellValue(base.formatValue(maxLocalAsymmetry));
				sheet.GetRow(num + i).CreateCell(19).SetCellValue(base.getLabelByCellCoordinate(cellLocalAsymmetry));
				sheet.GetRow(num + i).CreateCell(20).SetCellValue(preliminaryDiagnosis);
				sheet.GetRow(num + i).CreateCell(21).SetCellValue(finalDiagnosis);
			}
			FileStream fileStream = new FileStream(pathToExport, FileMode.Create);
			hSSFWorkbook.Write(fileStream);
			fileStream.Close();
		}

		private void drawTable(Sheet sheet, int x, int y, int rowCount, int columnCount, List<TermoReportItem> listOfItem)
		{
			string text = "Слева";
			string text2 = "Справа";
			CellStyle cellStyle = sheet.Workbook.CreateCellStyle();
			cellStyle.Alignment = HorizontalAlignment.RIGHT;
			cellStyle.VerticalAlignment = VerticalAlignment.CENTER;
			for (int i = 0; i < rowCount; i++)
			{
				sheet.CreateRow(y + i);
				for (int j = 0; j < columnCount; j++)
				{
					if (i == 0 && j != 0)
					{
						sheet.GetRow(y + i).CreateCell(x + j).SetCellValue(base.getLabelByCellIndex(j));
					}
					if (rowCount > 2 && j == 0)
					{
						string cellValue = string.Empty;
						if (i == 1)
						{
							cellValue = text2;
						}
						if (i == 2)
						{
							cellValue = text;
						}
						sheet.GetRow(y + i).CreateCell(x + j);
						sheet.GetRow(y + i).GetCell(x + j).CellStyle = cellStyle;
						sheet.GetRow(y + i).GetCell(x + j).SetCellValue(cellValue);
					}
				}
			}
			foreach (TermoReportItem current in listOfItem)
			{
				int num = y + 1;
				if (rowCount > 2 && current.GridX < 0)
				{
					num++;
				}
				int indexByCellCoordinate = base.getIndexByCellCoordinate(new Point(current.GridX, current.GridY));
				Cell cell = sheet.GetRow(num).GetCell(indexByCellCoordinate);
				if (cell == null)
				{
					sheet.GetRow(num).CreateCell(indexByCellCoordinate).SetCellValue(base.formatValue(current.ItemValue));
				}
				else
				{
					sheet.GetRow(num).GetCell(indexByCellCoordinate).SetCellValue(base.formatValue(current.ItemValue));
				}
			}
		}
	}
}
