using com.medical.termo.data;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace com.medical.termo.engine
{
	public class CreateReportEngine
	{
		private MeasureModel measureModel;

		public CreateReportEngine(MeasureModel measureModel)
		{
			this.measureModel = measureModel;
		}

		public TermoReport createReport()
		{
			TermoReport termoReport = new TermoReport();
			termoReport.DeltaTemp = this.measureModel.MaxTemp - this.measureModel.MinTemp;
			termoReport.AverageLeftTemp = this.getAverageLeftTemperature(this.measureModel);
			termoReport.AverageRightTemp = this.getAverageRightTemperature(this.measureModel);
			termoReport.AverageAsymmetry = Math.Abs(termoReport.AverageLeftTemp - termoReport.AverageRightTemp);
			termoReport.ListOfTemp = this.getListOfAverageTemperature(this.measureModel);
			termoReport.ListOfDeltaBetweenSymmetricPosition = this.getListOfDeltaBetweenSymmetricPosition(this.measureModel);
			termoReport.ListOfDeltaBetweenPositionAndAverage = this.getListOfDeltaBetweenAverageAtPositionAndAverageAtSide(this.measureModel, termoReport);
			termoReport.ListOfDeltaBetweenSymmetricSensor = this.getListOfMaxDeltaBetweenSymmetricSensor(this.measureModel);
			termoReport.ListOfDeltaBetweenSensorAndAverage = this.getListOfMaxDeltaBetweenSensorAndAverageAtSide(this.measureModel, termoReport);
			termoReport.ListOfDeltaBetweenSensorAndPosition = this.getListOfMaxDeltaBetweenSensorAndAverageAtPosition(this.measureModel);
			termoReport.CellMaxLocalUpperLeft = this.getMaxUpperLeftCell(termoReport);
			termoReport.CellMaxLocalUpperRight = this.getMaxUpperRightCell(termoReport);
			termoReport.MaxLocalUpperLeft = this.getMaxUpperLeftValue(termoReport);
			termoReport.MaxLocalUpperRight = this.getMaxUpperRightValue(termoReport);
			termoReport.MaxLocalAsymmetry = this.getMaxLocalAsymmetryValue(termoReport);
			termoReport.CellLocalAsymmetry = this.getMaxLocalAsymmetryCell(termoReport);
			return termoReport;
		}

		private float getAverageLeftTemperature(MeasureModel measureModel)
		{
			float num = 0f;
			int num2 = 0;
			foreach (Measure current in measureModel.Measures)
			{
				if (current.GridX < 0)
				{
					num += current.AverageTemp;
					num2++;
				}
			}
			return num / (float)num2;
		}

		private float getAverageRightTemperature(MeasureModel measureModel)
		{
			float num = 0f;
			int num2 = 0;
			foreach (Measure current in measureModel.Measures)
			{
				if (current.GridX > 0)
				{
					num += current.AverageTemp;
					num2++;
				}
			}
			return num / (float)num2;
		}

		private List<TermoReportItem> getListOfAverageTemperature(MeasureModel measureModel)
		{
			List<TermoReportItem> list = new List<TermoReportItem>();
			foreach (Measure current in measureModel.Measures)
			{
				list.Add(new TermoReportItem(current.GridX, current.GridY, current.AverageTemp));
			}
			return list;
		}

		private List<TermoReportItem> getListOfDeltaBetweenSymmetricPosition(MeasureModel measureModel)
		{
			List<TermoReportItem> list = new List<TermoReportItem>();
			foreach (Measure current in measureModel.Measures)
			{
				int num = measureModel.Measures.IndexOf(new Measure(-current.GridX, current.GridY));
				float itemValue = float.NegativeInfinity;
				if (num >= 0)
				{
					Measure measure = measureModel.Measures[num];
					itemValue = Math.Abs(current.AverageTemp - measure.AverageTemp);
				}
				list.Add(new TermoReportItem(current.GridX, current.GridY, itemValue));
			}
			return list;
		}

		private List<TermoReportItem> getListOfDeltaBetweenAverageAtPositionAndAverageAtSide(MeasureModel measureModel, TermoReport report)
		{
			List<TermoReportItem> list = new List<TermoReportItem>();
			foreach (Measure current in measureModel.Measures)
			{
				float itemValue;
				if (current.GridX < 0)
				{
					itemValue = current.AverageTemp - report.AverageLeftTemp;
				}
				else
				{
					itemValue = current.AverageTemp - report.AverageRightTemp;
				}
				list.Add(new TermoReportItem(current.GridX, current.GridY, itemValue));
			}
			return list;
		}

		private List<TermoReportItem> getListOfMaxDeltaBetweenSymmetricSensor(MeasureModel measureModel)
		{
			List<TermoReportItem> list = new List<TermoReportItem>();
			foreach (Measure current in measureModel.Measures)
			{
				int num = measureModel.Measures.IndexOf(new Measure(-current.GridX, current.GridY));
				float itemValue = float.NegativeInfinity;
				if (num >= 0)
				{
					float num2 = float.NegativeInfinity;
					Measure measure = measureModel.Measures[num];
					for (int i = 0; i < Settings.Current.SensorInColumn; i++)
					{
						for (int j = 0; j < Settings.Current.SensorInLine; j++)
						{
							float num3 = Math.Abs(current.getSensor(i, j) - measure.getSensor(Settings.Current.SensorInLine - i - 1, j));
							if (num3 > num2)
							{
								num2 = num3;
							}
						}
					}
					itemValue = num2;
				}
				list.Add(new TermoReportItem(current.GridX, current.GridY, itemValue));
			}
			return list;
		}

		private List<TermoReportItem> getListOfMaxDeltaBetweenSensorAndAverageAtSide(MeasureModel measureModel, TermoReport report)
		{
			List<TermoReportItem> list = new List<TermoReportItem>();
			foreach (Measure current in measureModel.Measures)
			{
				float num = float.NegativeInfinity;
				for (int i = 0; i < Settings.Current.SensorInColumn; i++)
				{
					for (int j = 0; j < Settings.Current.SensorInLine; j++)
					{
						float num2;
						if (current.GridX < 0)
						{
							num2 = current.getSensor(i, j) - report.AverageLeftTemp;
						}
						else
						{
							num2 = current.getSensor(i, j) - report.AverageRightTemp;
						}
						if (num2 > num)
						{
							num = num2;
						}
					}
				}
				list.Add(new TermoReportItem(current.GridX, current.GridY, num));
			}
			return list;
		}

		private List<TermoReportItem> getListOfMaxDeltaBetweenSensorAndAverageAtPosition(MeasureModel measureModel)
		{
			List<TermoReportItem> list = new List<TermoReportItem>();
			foreach (Measure current in measureModel.Measures)
			{
				float num = float.NegativeInfinity;
				for (int i = 0; i < Settings.Current.SensorInColumn; i++)
				{
					for (int j = 0; j < Settings.Current.SensorInLine; j++)
					{
						float num2 = current.getSensor(i, j) - current.AverageTemp;
						if (num2 > num)
						{
							num = num2;
						}
					}
				}
				list.Add(new TermoReportItem(current.GridX, current.GridY, num));
			}
			return list;
		}

		private Point getMaxUpperLeftCell(TermoReport report)
		{
			float num = float.NegativeInfinity;
			Point result = new Point(0, 0);
			foreach (TermoReportItem current in report.ListOfDeltaBetweenSensorAndAverage)
			{
				if (current.GridX < 0 && current.ItemValue > num)
				{
					num = current.ItemValue;
					result = new Point(current.GridX, current.GridY);
				}
			}
			return result;
		}

		private Point getMaxUpperRightCell(TermoReport report)
		{
			float num = float.NegativeInfinity;
			Point result = new Point(0, 0);
			foreach (TermoReportItem current in report.ListOfDeltaBetweenSensorAndAverage)
			{
				if (current.GridX > 0 && current.ItemValue > num)
				{
					num = current.ItemValue;
					result = new Point(current.GridX, current.GridY);
				}
			}
			return result;
		}

		private float getMaxUpperLeftValue(TermoReport report)
		{
			float num = float.NegativeInfinity;
			foreach (TermoReportItem current in report.ListOfDeltaBetweenSensorAndAverage)
			{
				if (current.GridX < 0 && current.ItemValue > num)
				{
					num = current.ItemValue;
				}
			}
			return num;
		}

		private float getMaxUpperRightValue(TermoReport report)
		{
			float num = float.NegativeInfinity;
			foreach (TermoReportItem current in report.ListOfDeltaBetweenSensorAndAverage)
			{
				if (current.GridX > 0 && current.ItemValue > num)
				{
					num = current.ItemValue;
				}
			}
			return num;
		}

		private float getMaxLocalAsymmetryValue(TermoReport report)
		{
			float num = float.NegativeInfinity;
			foreach (TermoReportItem current in report.ListOfDeltaBetweenSymmetricSensor)
			{
				if (current.ItemValue > num)
				{
					num = current.ItemValue;
				}
			}
			return num;
		}

		private Point getMaxLocalAsymmetryCell(TermoReport report)
		{
			float num = float.NegativeInfinity;
			Point result = new Point(0, 0);
			foreach (TermoReportItem current in report.ListOfDeltaBetweenSymmetricSensor)
			{
				if (current.ItemValue > num)
				{
					num = current.ItemValue;
					result = new Point(current.GridX, current.GridY);
				}
			}
			return result;
		}
	}
}
