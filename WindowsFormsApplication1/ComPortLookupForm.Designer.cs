﻿namespace WindowsFormsApplication1
{
    partial class ComPortLookupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comPortLookupProgressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // comPortLookupProgressBar
            // 
            this.comPortLookupProgressBar.Location = new System.Drawing.Point(12, 43);
            this.comPortLookupProgressBar.Name = "comPortLookupProgressBar";
            this.comPortLookupProgressBar.Size = new System.Drawing.Size(537, 23);
            this.comPortLookupProgressBar.TabIndex = 0;
            // 
            // ComPortLookupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 112);
            this.Controls.Add(this.comPortLookupProgressBar);
            this.Name = "ComPortLookupForm";
            this.Text = "Выполняется поиск устройства...";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar comPortLookupProgressBar;
    }
}