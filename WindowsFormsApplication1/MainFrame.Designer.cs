﻿namespace WindowsFormsApplication1
{
    partial class workpanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.workPanelTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.preliminaryDiagnosisLabel = new System.Windows.Forms.Label();
            this.preliminaryDiagnosisTextBox = new System.Windows.Forms.TextBox();
            this.doctorGroupBox = new System.Windows.Forms.GroupBox();
            this.hospitalLabel = new System.Windows.Forms.Label();
            this.hospitalTextBox = new System.Windows.Forms.TextBox();
            this.doctorMiddleNameLabel = new System.Windows.Forms.Label();
            this.doctorFirstNameLabel = new System.Windows.Forms.Label();
            this.doctorSecondNameLabel = new System.Windows.Forms.Label();
            this.doctorSecondNameTextBox = new System.Windows.Forms.TextBox();
            this.doctorMiddleNameTextBox = new System.Windows.Forms.TextBox();
            this.doctorFirstNameTextBox = new System.Windows.Forms.TextBox();
            this.patientGroupBox = new System.Windows.Forms.GroupBox();
            this.rightAxillaryTempNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.heightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.tempAxillaryLabel = new System.Windows.Forms.Label();
            this.weightLabel = new System.Windows.Forms.Label();
            this.weightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.rightAxillaryTempLabel = new System.Windows.Forms.Label();
            this.leftAxillaryTempLabel = new System.Windows.Forms.Label();
            this.leftAxillaryTempNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.environmentTempLabel = new System.Windows.Forms.Label();
            this.environmentTempNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.dateOfBirthDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dateOfBirthLabel = new System.Windows.Forms.Label();
            this.sexGroupBox = new System.Windows.Forms.GroupBox();
            this.sexFemaleRadioButton = new System.Windows.Forms.RadioButton();
            this.sexMaleRadioButton = new System.Windows.Forms.RadioButton();
            this.patientMiddleNameLabel = new System.Windows.Forms.Label();
            this.patientFirstNameLabel = new System.Windows.Forms.Label();
            this.patientSecondNameLabel = new System.Windows.Forms.Label();
            this.patientSecondNameTextBox = new System.Windows.Forms.TextBox();
            this.patientMiddleNameTextBox = new System.Windows.Forms.TextBox();
            this.patientFirstNameTextBox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lowerLevelLabel = new System.Windows.Forms.Label();
            this.upperLevelLabel = new System.Windows.Forms.Label();
            this.lowerLevelComboBox = new System.Windows.Forms.ComboBox();
            this.upperLevelComboBox = new System.Windows.Forms.ComboBox();
            this.warningLabel = new System.Windows.Forms.Label();
            this.timeLabelLabel = new System.Windows.Forms.Label();
            this.tempLabelLabel = new System.Windows.Forms.Label();
            this.tempLabel = new System.Windows.Forms.Label();
            this.tempCheckBox = new System.Windows.Forms.CheckBox();
            this.timerLabel = new System.Windows.Forms.Label();
            this.timeOfMeasureLabel = new System.Windows.Forms.Label();
            this.gridHeightLabel = new System.Windows.Forms.Label();
            this.gridWidthLabel = new System.Windows.Forms.Label();
            this.timeOfmeasureNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.gridHeightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.gridWidthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.plotPanel = new System.Windows.Forms.Panel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.termogramPictureBox = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.finalDiagnosisLabel = new System.Windows.Forms.Label();
            this.finalDiagnosisTextBox = new System.Windows.Forms.TextBox();
            this.exportButton = new System.Windows.Forms.Button();
            this.printButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.workPanelTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.doctorGroupBox.SuspendLayout();
            this.patientGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightAxillaryTempNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftAxillaryTempNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.environmentTempNumericUpDown)).BeginInit();
            this.sexGroupBox.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeOfmeasureNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHeightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridWidthNumericUpDown)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.termogramPictureBox)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // workPanelTabControl
            // 
            this.workPanelTabControl.AccessibleName = "";
            this.workPanelTabControl.AllowDrop = true;
            this.workPanelTabControl.Controls.Add(this.tabPage1);
            this.workPanelTabControl.Controls.Add(this.tabPage2);
            this.workPanelTabControl.Controls.Add(this.tabPage3);
            this.workPanelTabControl.Controls.Add(this.tabPage4);
            this.workPanelTabControl.Location = new System.Drawing.Point(12, 12);
            this.workPanelTabControl.Name = "workPanelTabControl";
            this.workPanelTabControl.SelectedIndex = 0;
            this.workPanelTabControl.Size = new System.Drawing.Size(489, 434);
            this.workPanelTabControl.TabIndex = 0;
            this.workPanelTabControl.SelectedIndexChanged += new System.EventHandler(this.workPanelTabControl_SelectedIndexChanged);
            this.workPanelTabControl.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.workPanelTabControl_Selecting);
            this.workPanelTabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.workPanelTabControl_Selected);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.preliminaryDiagnosisLabel);
            this.tabPage1.Controls.Add(this.preliminaryDiagnosisTextBox);
            this.tabPage1.Controls.Add(this.doctorGroupBox);
            this.tabPage1.Controls.Add(this.patientGroupBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(481, 408);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Параметры";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // preliminaryDiagnosisLabel
            // 
            this.preliminaryDiagnosisLabel.AutoSize = true;
            this.preliminaryDiagnosisLabel.Location = new System.Drawing.Point(245, 256);
            this.preliminaryDiagnosisLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.preliminaryDiagnosisLabel.Name = "preliminaryDiagnosisLabel";
            this.preliminaryDiagnosisLabel.Size = new System.Drawing.Size(165, 15);
            this.preliminaryDiagnosisLabel.TabIndex = 5;
            this.preliminaryDiagnosisLabel.Text = "Предварительный диагноз";
            // 
            // preliminaryDiagnosisTextBox
            // 
            this.preliminaryDiagnosisTextBox.Location = new System.Drawing.Point(247, 269);
            this.preliminaryDiagnosisTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.preliminaryDiagnosisTextBox.Multiline = true;
            this.preliminaryDiagnosisTextBox.Name = "preliminaryDiagnosisTextBox";
            this.preliminaryDiagnosisTextBox.Size = new System.Drawing.Size(211, 102);
            this.preliminaryDiagnosisTextBox.TabIndex = 6;
            // 
            // doctorGroupBox
            // 
            this.doctorGroupBox.Controls.Add(this.hospitalLabel);
            this.doctorGroupBox.Controls.Add(this.hospitalTextBox);
            this.doctorGroupBox.Controls.Add(this.doctorMiddleNameLabel);
            this.doctorGroupBox.Controls.Add(this.doctorFirstNameLabel);
            this.doctorGroupBox.Controls.Add(this.doctorSecondNameLabel);
            this.doctorGroupBox.Controls.Add(this.doctorSecondNameTextBox);
            this.doctorGroupBox.Controls.Add(this.doctorMiddleNameTextBox);
            this.doctorGroupBox.Controls.Add(this.doctorFirstNameTextBox);
            this.doctorGroupBox.Location = new System.Drawing.Point(247, 39);
            this.doctorGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this.doctorGroupBox.Name = "doctorGroupBox";
            this.doctorGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this.doctorGroupBox.Size = new System.Drawing.Size(210, 206);
            this.doctorGroupBox.TabIndex = 7;
            this.doctorGroupBox.TabStop = false;
            this.doctorGroupBox.Text = "Информация о враче";
            // 
            // hospitalLabel
            // 
            this.hospitalLabel.AutoSize = true;
            this.hospitalLabel.Location = new System.Drawing.Point(15, 149);
            this.hospitalLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.hospitalLabel.Name = "hospitalLabel";
            this.hospitalLabel.Size = new System.Drawing.Size(135, 15);
            this.hospitalLabel.TabIndex = 6;
            this.hospitalLabel.Text = "Лечебное учреждение";
            // 
            // hospitalTextBox
            // 
            this.hospitalTextBox.Location = new System.Drawing.Point(17, 162);
            this.hospitalTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.hospitalTextBox.Name = "hospitalTextBox";
            this.hospitalTextBox.Size = new System.Drawing.Size(174, 20);
            this.hospitalTextBox.TabIndex = 7;
            // 
            // doctorMiddleNameLabel
            // 
            this.doctorMiddleNameLabel.AutoSize = true;
            this.doctorMiddleNameLabel.Location = new System.Drawing.Point(15, 106);
            this.doctorMiddleNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.doctorMiddleNameLabel.Name = "doctorMiddleNameLabel";
            this.doctorMiddleNameLabel.Size = new System.Drawing.Size(63, 15);
            this.doctorMiddleNameLabel.TabIndex = 4;
            this.doctorMiddleNameLabel.Text = "Отчество";
            // 
            // doctorFirstNameLabel
            // 
            this.doctorFirstNameLabel.AutoSize = true;
            this.doctorFirstNameLabel.Location = new System.Drawing.Point(15, 65);
            this.doctorFirstNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.doctorFirstNameLabel.Name = "doctorFirstNameLabel";
            this.doctorFirstNameLabel.Size = new System.Drawing.Size(32, 15);
            this.doctorFirstNameLabel.TabIndex = 2;
            this.doctorFirstNameLabel.Text = "Имя";
            // 
            // doctorSecondNameLabel
            // 
            this.doctorSecondNameLabel.AutoSize = true;
            this.doctorSecondNameLabel.Location = new System.Drawing.Point(15, 28);
            this.doctorSecondNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.doctorSecondNameLabel.Name = "doctorSecondNameLabel";
            this.doctorSecondNameLabel.Size = new System.Drawing.Size(62, 15);
            this.doctorSecondNameLabel.TabIndex = 0;
            this.doctorSecondNameLabel.Text = "Фамилия";
            // 
            // doctorSecondNameTextBox
            // 
            this.doctorSecondNameTextBox.Location = new System.Drawing.Point(17, 41);
            this.doctorSecondNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.doctorSecondNameTextBox.Name = "doctorSecondNameTextBox";
            this.doctorSecondNameTextBox.Size = new System.Drawing.Size(174, 20);
            this.doctorSecondNameTextBox.TabIndex = 1;
            // 
            // doctorMiddleNameTextBox
            // 
            this.doctorMiddleNameTextBox.Location = new System.Drawing.Point(17, 119);
            this.doctorMiddleNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.doctorMiddleNameTextBox.Name = "doctorMiddleNameTextBox";
            this.doctorMiddleNameTextBox.Size = new System.Drawing.Size(174, 20);
            this.doctorMiddleNameTextBox.TabIndex = 5;
            // 
            // doctorFirstNameTextBox
            // 
            this.doctorFirstNameTextBox.Location = new System.Drawing.Point(17, 78);
            this.doctorFirstNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.doctorFirstNameTextBox.Name = "doctorFirstNameTextBox";
            this.doctorFirstNameTextBox.Size = new System.Drawing.Size(174, 20);
            this.doctorFirstNameTextBox.TabIndex = 3;
            // 
            // patientGroupBox
            // 
            this.patientGroupBox.Controls.Add(this.rightAxillaryTempNumericUpDown);
            this.patientGroupBox.Controls.Add(this.label2);
            this.patientGroupBox.Controls.Add(this.heightNumericUpDown);
            this.patientGroupBox.Controls.Add(this.tempAxillaryLabel);
            this.patientGroupBox.Controls.Add(this.weightLabel);
            this.patientGroupBox.Controls.Add(this.weightNumericUpDown);
            this.patientGroupBox.Controls.Add(this.rightAxillaryTempLabel);
            this.patientGroupBox.Controls.Add(this.leftAxillaryTempLabel);
            this.patientGroupBox.Controls.Add(this.leftAxillaryTempNumericUpDown);
            this.patientGroupBox.Controls.Add(this.environmentTempLabel);
            this.patientGroupBox.Controls.Add(this.environmentTempNumericUpDown);
            this.patientGroupBox.Controls.Add(this.dateOfBirthDateTimePicker);
            this.patientGroupBox.Controls.Add(this.dateOfBirthLabel);
            this.patientGroupBox.Controls.Add(this.sexGroupBox);
            this.patientGroupBox.Controls.Add(this.patientMiddleNameLabel);
            this.patientGroupBox.Controls.Add(this.patientFirstNameLabel);
            this.patientGroupBox.Controls.Add(this.patientSecondNameLabel);
            this.patientGroupBox.Controls.Add(this.patientSecondNameTextBox);
            this.patientGroupBox.Controls.Add(this.patientMiddleNameTextBox);
            this.patientGroupBox.Controls.Add(this.patientFirstNameTextBox);
            this.patientGroupBox.Location = new System.Drawing.Point(26, 39);
            this.patientGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this.patientGroupBox.Name = "patientGroupBox";
            this.patientGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this.patientGroupBox.Size = new System.Drawing.Size(210, 332);
            this.patientGroupBox.TabIndex = 4;
            this.patientGroupBox.TabStop = false;
            this.patientGroupBox.Text = "Информация о пациенте";
            // 
            // rightAxillaryTempNumericUpDown
            // 
            this.rightAxillaryTempNumericUpDown.DecimalPlaces = 1;
            this.rightAxillaryTempNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.rightAxillaryTempNumericUpDown.Location = new System.Drawing.Point(92, 253);
            this.rightAxillaryTempNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.rightAxillaryTempNumericUpDown.Name = "rightAxillaryTempNumericUpDown";
            this.rightAxillaryTempNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.rightAxillaryTempNumericUpDown.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(145, 284);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 19;
            this.label2.Text = "Рост, cм";
            // 
            // heightNumericUpDown
            // 
            this.heightNumericUpDown.Location = new System.Drawing.Point(145, 298);
            this.heightNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.heightNumericUpDown.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.heightNumericUpDown.Name = "heightNumericUpDown";
            this.heightNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.heightNumericUpDown.TabIndex = 18;
            // 
            // tempAxillaryLabel
            // 
            this.tempAxillaryLabel.Location = new System.Drawing.Point(14, 247);
            this.tempAxillaryLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tempAxillaryLabel.Name = "tempAxillaryLabel";
            this.tempAxillaryLabel.Size = new System.Drawing.Size(78, 31);
            this.tempAxillaryLabel.TabIndex = 17;
            this.tempAxillaryLabel.Text = "Т-ра аксиллярно";
            // 
            // weightLabel
            // 
            this.weightLabel.AutoSize = true;
            this.weightLabel.Location = new System.Drawing.Point(90, 285);
            this.weightLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.weightLabel.Name = "weightLabel";
            this.weightLabel.Size = new System.Drawing.Size(45, 15);
            this.weightLabel.TabIndex = 16;
            this.weightLabel.Text = "Вес, кг";
            // 
            // weightNumericUpDown
            // 
            this.weightNumericUpDown.DecimalPlaces = 1;
            this.weightNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.weightNumericUpDown.Location = new System.Drawing.Point(92, 298);
            this.weightNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.weightNumericUpDown.Name = "weightNumericUpDown";
            this.weightNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.weightNumericUpDown.TabIndex = 15;
            // 
            // rightAxillaryTempLabel
            // 
            this.rightAxillaryTempLabel.AutoSize = true;
            this.rightAxillaryTempLabel.Location = new System.Drawing.Point(90, 240);
            this.rightAxillaryTempLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.rightAxillaryTempLabel.Name = "rightAxillaryTempLabel";
            this.rightAxillaryTempLabel.Size = new System.Drawing.Size(43, 15);
            this.rightAxillaryTempLabel.TabIndex = 14;
            this.rightAxillaryTempLabel.Text = "Слева";
            // 
            // leftAxillaryTempLabel
            // 
            this.leftAxillaryTempLabel.AutoSize = true;
            this.leftAxillaryTempLabel.Location = new System.Drawing.Point(142, 240);
            this.leftAxillaryTempLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.leftAxillaryTempLabel.Name = "leftAxillaryTempLabel";
            this.leftAxillaryTempLabel.Size = new System.Drawing.Size(50, 15);
            this.leftAxillaryTempLabel.TabIndex = 12;
            this.leftAxillaryTempLabel.Text = "Справа";
            // 
            // leftAxillaryTempNumericUpDown
            // 
            this.leftAxillaryTempNumericUpDown.DecimalPlaces = 1;
            this.leftAxillaryTempNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.leftAxillaryTempNumericUpDown.Location = new System.Drawing.Point(145, 253);
            this.leftAxillaryTempNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.leftAxillaryTempNumericUpDown.Name = "leftAxillaryTempNumericUpDown";
            this.leftAxillaryTempNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.leftAxillaryTempNumericUpDown.TabIndex = 11;
            // 
            // environmentTempLabel
            // 
            this.environmentTempLabel.AutoSize = true;
            this.environmentTempLabel.Location = new System.Drawing.Point(14, 285);
            this.environmentTempLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.environmentTempLabel.Name = "environmentTempLabel";
            this.environmentTempLabel.Size = new System.Drawing.Size(97, 15);
            this.environmentTempLabel.TabIndex = 10;
            this.environmentTempLabel.Text = "Т-ра окр. среды";
            // 
            // environmentTempNumericUpDown
            // 
            this.environmentTempNumericUpDown.DecimalPlaces = 1;
            this.environmentTempNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.environmentTempNumericUpDown.Location = new System.Drawing.Point(16, 298);
            this.environmentTempNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.environmentTempNumericUpDown.Name = "environmentTempNumericUpDown";
            this.environmentTempNumericUpDown.Size = new System.Drawing.Size(64, 20);
            this.environmentTempNumericUpDown.TabIndex = 9;
            // 
            // dateOfBirthDateTimePicker
            // 
            this.dateOfBirthDateTimePicker.Location = new System.Drawing.Point(16, 212);
            this.dateOfBirthDateTimePicker.Margin = new System.Windows.Forms.Padding(2);
            this.dateOfBirthDateTimePicker.Name = "dateOfBirthDateTimePicker";
            this.dateOfBirthDateTimePicker.Size = new System.Drawing.Size(174, 20);
            this.dateOfBirthDateTimePicker.TabIndex = 8;
            // 
            // dateOfBirthLabel
            // 
            this.dateOfBirthLabel.AutoSize = true;
            this.dateOfBirthLabel.Location = new System.Drawing.Point(16, 196);
            this.dateOfBirthLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dateOfBirthLabel.Name = "dateOfBirthLabel";
            this.dateOfBirthLabel.Size = new System.Drawing.Size(98, 15);
            this.dateOfBirthLabel.TabIndex = 7;
            this.dateOfBirthLabel.Text = "Дата рождения";
            // 
            // sexGroupBox
            // 
            this.sexGroupBox.Controls.Add(this.sexFemaleRadioButton);
            this.sexGroupBox.Controls.Add(this.sexMaleRadioButton);
            this.sexGroupBox.Location = new System.Drawing.Point(16, 149);
            this.sexGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this.sexGroupBox.Name = "sexGroupBox";
            this.sexGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this.sexGroupBox.Size = new System.Drawing.Size(180, 42);
            this.sexGroupBox.TabIndex = 6;
            this.sexGroupBox.TabStop = false;
            this.sexGroupBox.Text = "Пол";
            // 
            // sexFemaleRadioButton
            // 
            this.sexFemaleRadioButton.AutoSize = true;
            this.sexFemaleRadioButton.Location = new System.Drawing.Point(104, 15);
            this.sexFemaleRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.sexFemaleRadioButton.Name = "sexFemaleRadioButton";
            this.sexFemaleRadioButton.Size = new System.Drawing.Size(79, 19);
            this.sexFemaleRadioButton.TabIndex = 1;
            this.sexFemaleRadioButton.Text = "Женский";
            this.sexFemaleRadioButton.UseVisualStyleBackColor = true;
            // 
            // sexMaleRadioButton
            // 
            this.sexMaleRadioButton.AutoSize = true;
            this.sexMaleRadioButton.Checked = true;
            this.sexMaleRadioButton.Location = new System.Drawing.Point(11, 15);
            this.sexMaleRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.sexMaleRadioButton.Name = "sexMaleRadioButton";
            this.sexMaleRadioButton.Size = new System.Drawing.Size(79, 19);
            this.sexMaleRadioButton.TabIndex = 0;
            this.sexMaleRadioButton.TabStop = true;
            this.sexMaleRadioButton.Text = "Мужской";
            this.sexMaleRadioButton.UseVisualStyleBackColor = true;
            // 
            // patientMiddleNameLabel
            // 
            this.patientMiddleNameLabel.AutoSize = true;
            this.patientMiddleNameLabel.Location = new System.Drawing.Point(14, 106);
            this.patientMiddleNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientMiddleNameLabel.Name = "patientMiddleNameLabel";
            this.patientMiddleNameLabel.Size = new System.Drawing.Size(63, 15);
            this.patientMiddleNameLabel.TabIndex = 4;
            this.patientMiddleNameLabel.Text = "Отчество";
            // 
            // patientFirstNameLabel
            // 
            this.patientFirstNameLabel.AutoSize = true;
            this.patientFirstNameLabel.Location = new System.Drawing.Point(14, 65);
            this.patientFirstNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientFirstNameLabel.Name = "patientFirstNameLabel";
            this.patientFirstNameLabel.Size = new System.Drawing.Size(32, 15);
            this.patientFirstNameLabel.TabIndex = 2;
            this.patientFirstNameLabel.Text = "Имя";
            // 
            // patientSecondNameLabel
            // 
            this.patientSecondNameLabel.AutoSize = true;
            this.patientSecondNameLabel.Location = new System.Drawing.Point(14, 28);
            this.patientSecondNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.patientSecondNameLabel.Name = "patientSecondNameLabel";
            this.patientSecondNameLabel.Size = new System.Drawing.Size(62, 15);
            this.patientSecondNameLabel.TabIndex = 0;
            this.patientSecondNameLabel.Text = "Фамилия";
            // 
            // patientSecondNameTextBox
            // 
            this.patientSecondNameTextBox.Location = new System.Drawing.Point(16, 41);
            this.patientSecondNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.patientSecondNameTextBox.Name = "patientSecondNameTextBox";
            this.patientSecondNameTextBox.Size = new System.Drawing.Size(174, 20);
            this.patientSecondNameTextBox.TabIndex = 1;
            // 
            // patientMiddleNameTextBox
            // 
            this.patientMiddleNameTextBox.Location = new System.Drawing.Point(16, 119);
            this.patientMiddleNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.patientMiddleNameTextBox.Name = "patientMiddleNameTextBox";
            this.patientMiddleNameTextBox.Size = new System.Drawing.Size(174, 20);
            this.patientMiddleNameTextBox.TabIndex = 5;
            // 
            // patientFirstNameTextBox
            // 
            this.patientFirstNameTextBox.Location = new System.Drawing.Point(16, 78);
            this.patientFirstNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.patientFirstNameTextBox.Name = "patientFirstNameTextBox";
            this.patientFirstNameTextBox.Size = new System.Drawing.Size(174, 20);
            this.patientFirstNameTextBox.TabIndex = 3;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lowerLevelLabel);
            this.tabPage2.Controls.Add(this.upperLevelLabel);
            this.tabPage2.Controls.Add(this.lowerLevelComboBox);
            this.tabPage2.Controls.Add(this.upperLevelComboBox);
            this.tabPage2.Controls.Add(this.warningLabel);
            this.tabPage2.Controls.Add(this.timeLabelLabel);
            this.tabPage2.Controls.Add(this.tempLabelLabel);
            this.tabPage2.Controls.Add(this.tempLabel);
            this.tabPage2.Controls.Add(this.tempCheckBox);
            this.tabPage2.Controls.Add(this.timerLabel);
            this.tabPage2.Controls.Add(this.timeOfMeasureLabel);
            this.tabPage2.Controls.Add(this.gridHeightLabel);
            this.tabPage2.Controls.Add(this.gridWidthLabel);
            this.tabPage2.Controls.Add(this.timeOfmeasureNumericUpDown);
            this.tabPage2.Controls.Add(this.gridHeightNumericUpDown);
            this.tabPage2.Controls.Add(this.gridWidthNumericUpDown);
            this.tabPage2.Controls.Add(this.plotPanel);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(481, 408);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Измерение температуры";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // lowerLevelLabel
            // 
            this.lowerLevelLabel.AutoSize = true;
            this.lowerLevelLabel.Location = new System.Drawing.Point(321, 295);
            this.lowerLevelLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lowerLevelLabel.Name = "lowerLevelLabel";
            this.lowerLevelLabel.Size = new System.Drawing.Size(103, 15);
            this.lowerLevelLabel.TabIndex = 32;
            this.lowerLevelLabel.Text = "Нижний уровень";
            // 
            // upperLevelLabel
            // 
            this.upperLevelLabel.AutoSize = true;
            this.upperLevelLabel.Location = new System.Drawing.Point(324, 15);
            this.upperLevelLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.upperLevelLabel.Name = "upperLevelLabel";
            this.upperLevelLabel.Size = new System.Drawing.Size(106, 15);
            this.upperLevelLabel.TabIndex = 31;
            this.upperLevelLabel.Text = "Верхний уровень";
            // 
            // lowerLevelComboBox
            // 
            this.lowerLevelComboBox.FormattingEnabled = true;
            this.lowerLevelComboBox.Location = new System.Drawing.Point(324, 311);
            this.lowerLevelComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.lowerLevelComboBox.Name = "lowerLevelComboBox";
            this.lowerLevelComboBox.Size = new System.Drawing.Size(92, 21);
            this.lowerLevelComboBox.TabIndex = 30;
            this.lowerLevelComboBox.SelectedValueChanged += new System.EventHandler(this.upperLevelComboBox_SelectedValueChanged);
            // 
            // upperLevelComboBox
            // 
            this.upperLevelComboBox.FormattingEnabled = true;
            this.upperLevelComboBox.Location = new System.Drawing.Point(324, 33);
            this.upperLevelComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.upperLevelComboBox.Name = "upperLevelComboBox";
            this.upperLevelComboBox.Size = new System.Drawing.Size(92, 21);
            this.upperLevelComboBox.TabIndex = 29;
            this.upperLevelComboBox.SelectedValueChanged += new System.EventHandler(this.upperLevelComboBox_SelectedValueChanged);
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = true;
            this.warningLabel.Location = new System.Drawing.Point(14, 344);
            this.warningLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(529, 15);
            this.warningLabel.TabIndex = 28;
            this.warningLabel.Text = "Для замера нажмите кнопку на блоке датчиков и дождитесь окончания отсчета таймера" +
    "";
            // 
            // timeLabelLabel
            // 
            this.timeLabelLabel.AutoSize = true;
            this.timeLabelLabel.Location = new System.Drawing.Point(316, 222);
            this.timeLabelLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.timeLabelLabel.Name = "timeLabelLabel";
            this.timeLabelLabel.Size = new System.Drawing.Size(126, 15);
            this.timeLabelLabel.TabIndex = 27;
            this.timeLabelLabel.Text = "Осталось до замера";
            // 
            // tempLabelLabel
            // 
            this.tempLabelLabel.AutoSize = true;
            this.tempLabelLabel.Location = new System.Drawing.Point(314, 168);
            this.tempLabelLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tempLabelLabel.Name = "tempLabelLabel";
            this.tempLabelLabel.Size = new System.Drawing.Size(130, 15);
            this.tempLabelLabel.TabIndex = 26;
            this.tempLabelLabel.Text = "Температура в точке";
            // 
            // tempLabel
            // 
            this.tempLabel.AutoSize = true;
            this.tempLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tempLabel.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tempLabel.Location = new System.Drawing.Point(329, 182);
            this.tempLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tempLabel.Name = "tempLabel";
            this.tempLabel.Size = new System.Drawing.Size(2, 36);
            this.tempLabel.TabIndex = 17;
            // 
            // tempCheckBox
            // 
            this.tempCheckBox.AutoSize = true;
            this.tempCheckBox.Location = new System.Drawing.Point(319, 276);
            this.tempCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.tempCheckBox.Name = "tempCheckBox";
            this.tempCheckBox.Size = new System.Drawing.Size(181, 19);
            this.tempCheckBox.TabIndex = 25;
            this.tempCheckBox.Text = "Отображать температуру";
            this.tempCheckBox.UseVisualStyleBackColor = true;
            // 
            // timerLabel
            // 
            this.timerLabel.AutoSize = true;
            this.timerLabel.BackColor = System.Drawing.Color.White;
            this.timerLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.timerLabel.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.timerLabel.ForeColor = System.Drawing.Color.Black;
            this.timerLabel.Location = new System.Drawing.Point(330, 245);
            this.timerLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.timerLabel.Name = "timerLabel";
            this.timerLabel.Size = new System.Drawing.Size(2, 36);
            this.timerLabel.TabIndex = 24;
            this.timerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timeOfMeasureLabel
            // 
            this.timeOfMeasureLabel.AutoSize = true;
            this.timeOfMeasureLabel.Location = new System.Drawing.Point(326, 120);
            this.timeOfMeasureLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.timeOfMeasureLabel.Name = "timeOfMeasureLabel";
            this.timeOfMeasureLabel.Size = new System.Drawing.Size(124, 15);
            this.timeOfMeasureLabel.TabIndex = 23;
            this.timeOfMeasureLabel.Text = "Время измерения, с";
            // 
            // gridHeightLabel
            // 
            this.gridHeightLabel.AutoSize = true;
            this.gridHeightLabel.Location = new System.Drawing.Point(326, 88);
            this.gridHeightLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.gridHeightLabel.Name = "gridHeightLabel";
            this.gridHeightLabel.Size = new System.Drawing.Size(104, 15);
            this.gridHeightLabel.TabIndex = 22;
            this.gridHeightLabel.Text = "Высота решетки";
            // 
            // gridWidthLabel
            // 
            this.gridWidthLabel.AutoSize = true;
            this.gridWidthLabel.Location = new System.Drawing.Point(326, 56);
            this.gridWidthLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.gridWidthLabel.Name = "gridWidthLabel";
            this.gridWidthLabel.Size = new System.Drawing.Size(106, 15);
            this.gridWidthLabel.TabIndex = 21;
            this.gridWidthLabel.Text = "Ширина решетки";
            // 
            // timeOfmeasureNumericUpDown
            // 
            this.timeOfmeasureNumericUpDown.Location = new System.Drawing.Point(328, 133);
            this.timeOfmeasureNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.timeOfmeasureNumericUpDown.Name = "timeOfmeasureNumericUpDown";
            this.timeOfmeasureNumericUpDown.ReadOnly = true;
            this.timeOfmeasureNumericUpDown.Size = new System.Drawing.Size(67, 20);
            this.timeOfmeasureNumericUpDown.TabIndex = 20;
            // 
            // gridHeightNumericUpDown
            // 
            this.gridHeightNumericUpDown.Location = new System.Drawing.Point(328, 101);
            this.gridHeightNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.gridHeightNumericUpDown.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.gridHeightNumericUpDown.Name = "gridHeightNumericUpDown";
            this.gridHeightNumericUpDown.ReadOnly = true;
            this.gridHeightNumericUpDown.Size = new System.Drawing.Size(67, 20);
            this.gridHeightNumericUpDown.TabIndex = 19;
            this.gridHeightNumericUpDown.ValueChanged += new System.EventHandler(this.gridHeightNumericUpDown_ValueChanged);
            // 
            // gridWidthNumericUpDown
            // 
            this.gridWidthNumericUpDown.Location = new System.Drawing.Point(328, 69);
            this.gridWidthNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.gridWidthNumericUpDown.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.gridWidthNumericUpDown.Name = "gridWidthNumericUpDown";
            this.gridWidthNumericUpDown.ReadOnly = true;
            this.gridWidthNumericUpDown.Size = new System.Drawing.Size(67, 20);
            this.gridWidthNumericUpDown.TabIndex = 18;
            this.gridWidthNumericUpDown.ValueChanged += new System.EventHandler(this.gridWidthNumericUpDown_ValueChanged);
            // 
            // plotPanel
            // 
            this.plotPanel.BackColor = System.Drawing.SystemColors.Window;
            this.plotPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plotPanel.Location = new System.Drawing.Point(17, 15);
            this.plotPanel.Margin = new System.Windows.Forms.Padding(2);
            this.plotPanel.Name = "plotPanel";
            this.plotPanel.Size = new System.Drawing.Size(293, 317);
            this.plotPanel.TabIndex = 16;
            this.plotPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.plotPanel_Paint);
            this.plotPanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.plotPanel_MouseClick);
            this.plotPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.plotPanel_MouseMove);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.termogramPictureBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(481, 408);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Результаты";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // termogramPictureBox
            // 
            this.termogramPictureBox.Location = new System.Drawing.Point(24, 5);
            this.termogramPictureBox.Name = "termogramPictureBox";
            this.termogramPictureBox.Size = new System.Drawing.Size(420, 420);
            this.termogramPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.termogramPictureBox.TabIndex = 1;
            this.termogramPictureBox.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.finalDiagnosisLabel);
            this.tabPage4.Controls.Add(this.finalDiagnosisTextBox);
            this.tabPage4.Controls.Add(this.exportButton);
            this.tabPage4.Controls.Add(this.printButton);
            this.tabPage4.Controls.Add(this.saveButton);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(481, 408);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Сохранение и экспорт";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // finalDiagnosisLabel
            // 
            this.finalDiagnosisLabel.AutoSize = true;
            this.finalDiagnosisLabel.Location = new System.Drawing.Point(21, 3);
            this.finalDiagnosisLabel.Name = "finalDiagnosisLabel";
            this.finalDiagnosisLabel.Size = new System.Drawing.Size(149, 15);
            this.finalDiagnosisLabel.TabIndex = 9;
            this.finalDiagnosisLabel.Text = "Окончательный диагноз";
            // 
            // finalDiagnosisTextBox
            // 
            this.finalDiagnosisTextBox.Location = new System.Drawing.Point(21, 22);
            this.finalDiagnosisTextBox.Multiline = true;
            this.finalDiagnosisTextBox.Name = "finalDiagnosisTextBox";
            this.finalDiagnosisTextBox.Size = new System.Drawing.Size(435, 247);
            this.finalDiagnosisTextBox.TabIndex = 8;
            // 
            // exportButton
            // 
            this.exportButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.exportButton.Location = new System.Drawing.Point(21, 364);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(435, 38);
            this.exportButton.TabIndex = 7;
            this.exportButton.Text = "Экспорт";
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click_1);
            // 
            // printButton
            // 
            this.printButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.printButton.Location = new System.Drawing.Point(21, 321);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(435, 37);
            this.printButton.TabIndex = 6;
            this.printButton.Text = "Печать";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click_1);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(21, 275);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(435, 40);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "Сохранить";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click_1);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(15, 452);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(212, 41);
            this.button1.TabIndex = 1;
            this.button1.Text = "Назад";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(265, 452);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(233, 41);
            this.button2.TabIndex = 2;
            this.button2.Text = "Вперед";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // workpanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 501);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.workPanelTabControl);
            this.MaximumSize = new System.Drawing.Size(524, 548);
            this.MinimumSize = new System.Drawing.Size(524, 548);
            this.Name = "workpanel";
            this.Text = "Termo 2.1";
            this.Load += new System.EventHandler(this._1_Load);
            this.workPanelTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.doctorGroupBox.ResumeLayout(false);
            this.doctorGroupBox.PerformLayout();
            this.patientGroupBox.ResumeLayout(false);
            this.patientGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightAxillaryTempNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftAxillaryTempNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.environmentTempNumericUpDown)).EndInit();
            this.sexGroupBox.ResumeLayout(false);
            this.sexGroupBox.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeOfmeasureNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHeightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridWidthNumericUpDown)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.termogramPictureBox)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl workPanelTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label lowerLevelLabel;
        private System.Windows.Forms.Label upperLevelLabel;
        private System.Windows.Forms.ComboBox lowerLevelComboBox;
        private System.Windows.Forms.ComboBox upperLevelComboBox;
        private System.Windows.Forms.Label warningLabel;
        private System.Windows.Forms.Label timeLabelLabel;
        private System.Windows.Forms.Label tempLabelLabel;
        private System.Windows.Forms.Label tempLabel;
        private System.Windows.Forms.CheckBox tempCheckBox;
        private System.Windows.Forms.Label timerLabel;
        private System.Windows.Forms.Label timeOfMeasureLabel;
        private System.Windows.Forms.Label gridHeightLabel;
        private System.Windows.Forms.Label gridWidthLabel;
        private System.Windows.Forms.NumericUpDown timeOfmeasureNumericUpDown;
        private System.Windows.Forms.NumericUpDown gridHeightNumericUpDown;
        private System.Windows.Forms.NumericUpDown gridWidthNumericUpDown;
        private System.Windows.Forms.Panel plotPanel;
        private System.Windows.Forms.Label preliminaryDiagnosisLabel;
        private System.Windows.Forms.TextBox preliminaryDiagnosisTextBox;
        private System.Windows.Forms.GroupBox doctorGroupBox;
        private System.Windows.Forms.Label hospitalLabel;
        private System.Windows.Forms.TextBox hospitalTextBox;
        private System.Windows.Forms.Label doctorMiddleNameLabel;
        private System.Windows.Forms.Label doctorFirstNameLabel;
        private System.Windows.Forms.Label doctorSecondNameLabel;
        private System.Windows.Forms.TextBox doctorSecondNameTextBox;
        private System.Windows.Forms.TextBox doctorMiddleNameTextBox;
        private System.Windows.Forms.TextBox doctorFirstNameTextBox;
        private System.Windows.Forms.GroupBox patientGroupBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown heightNumericUpDown;
        private System.Windows.Forms.Label tempAxillaryLabel;
        private System.Windows.Forms.Label weightLabel;
        private System.Windows.Forms.NumericUpDown weightNumericUpDown;
        private System.Windows.Forms.Label rightAxillaryTempLabel;
        private System.Windows.Forms.NumericUpDown rightAxillaryTempNumericUpDown;
        private System.Windows.Forms.Label leftAxillaryTempLabel;
        private System.Windows.Forms.NumericUpDown leftAxillaryTempNumericUpDown;
        private System.Windows.Forms.Label environmentTempLabel;
        private System.Windows.Forms.NumericUpDown environmentTempNumericUpDown;
        private System.Windows.Forms.DateTimePicker dateOfBirthDateTimePicker;
        private System.Windows.Forms.Label dateOfBirthLabel;
        private System.Windows.Forms.GroupBox sexGroupBox;
        private System.Windows.Forms.RadioButton sexFemaleRadioButton;
        private System.Windows.Forms.RadioButton sexMaleRadioButton;
        private System.Windows.Forms.Label patientMiddleNameLabel;
        private System.Windows.Forms.Label patientFirstNameLabel;
        private System.Windows.Forms.Label patientSecondNameLabel;
        private System.Windows.Forms.TextBox patientSecondNameTextBox;
        private System.Windows.Forms.TextBox patientMiddleNameTextBox;
        private System.Windows.Forms.TextBox patientFirstNameTextBox;
        private System.Windows.Forms.PictureBox termogramPictureBox;
        private System.Windows.Forms.Label finalDiagnosisLabel;
        private System.Windows.Forms.TextBox finalDiagnosisTextBox;
        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}