using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Termo.Properties
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
	internal class Resources
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(Resources.resourceMan, null))
				{
					ResourceManager resourceManager = new ResourceManager("Termo.Properties.Resources", typeof(Resources).Assembly);
					Resources.resourceMan = resourceManager;
				}
				return Resources.resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return Resources.resourceCulture;
			}
			set
			{
				Resources.resourceCulture = value;
			}
		}

		internal static Bitmap Apply
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Apply", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Browse
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Browse", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Callibrate
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Callibrate", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Close
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Close", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Exit
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Exit", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Export
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Export", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Help
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Help", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Lookup
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Lookup", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Next
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Next", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Open
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Open", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Preview
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Preview", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Print
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Print", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Report
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Report", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Save
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Save", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Settings
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Settings", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap StartProject
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("StartProject", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap StopProject
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("StopProject", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Vertebra
		{
			get
			{
				object @object = Resources.ResourceManager.GetObject("Vertebra", Resources.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal Resources()
		{
		}
	}
}
